import 'package:email_validator/email_validator.dart';

enum Validation{
  empty, email
}

class ValidationHelper {
  static dynamic isEmpty(dynamic value, [String label = ""]) => value.toString().isEmpty ?  "$label tidak boleh kosong" : null;
  static dynamic validateEmail(String email, [String label = ""]) => !EmailValidator.validate(email) ? "$label tidak valid" : null;
}