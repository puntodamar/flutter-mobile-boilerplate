import 'package:boilerplate/helpers/color_helper.dart';
import 'package:flutter/material.dart';
import 'package:boilerplate/helpers/extensions.dart';
import 'package:get/get.dart';

class TypographyHelper {
  static TextStyle headline1() => TextStyle(
      fontSize: 11.735.fz,
      color: ColorHelper.materialColor.shade900,
      fontWeight: FontWeight.w300,
      letterSpacing: -1.5,
      decoration: TextDecoration.none);

  static TextStyle headline2() => TextStyle(
      fontSize: 7.35.fz,
      color: ColorHelper.materialColor.shade900,
      fontWeight: FontWeight.w300,
      letterSpacing: -0.5,
      decoration: TextDecoration.none);

  static TextStyle headline3() => TextStyle(
      fontSize: 5.865.fz,
      color: ColorHelper.materialColor.shade900,
      fontWeight: FontWeight.w400,
      letterSpacing: 0.0,
      decoration: TextDecoration.none);

  static TextStyle headline4() => TextStyle(
      fontSize: 4.16.fz,
      color: ColorHelper.materialColor.shade900,
      fontWeight: FontWeight.w400,
      letterSpacing: 0.25,
      decoration: TextDecoration.none);

  static TextStyle headline5() => TextStyle(
      fontSize: 2.93.fz,
      color: ColorHelper.materialColor.shade900,
      fontWeight: FontWeight.w400,
      letterSpacing: 0.0,
      decoration: TextDecoration.none);

  static TextStyle headline6() => TextStyle(
      fontSize: 2.45.fz,
      color: ColorHelper.materialColor.shade900,
      fontWeight: FontWeight.w500,
      letterSpacing: 0.15,
      decoration: TextDecoration.none);

  static TextStyle subtitle1() => TextStyle(
      fontSize: 1.955.fz,
      color: ColorHelper.materialColor.shade900,
      fontWeight: FontWeight.w400,
      letterSpacing: 0.15,
      decoration: TextDecoration.none);

  static TextStyle subtitle2() => TextStyle(
      fontSize: 1.711.fz,
      color: ColorHelper.materialColor.shade900,
      fontWeight: FontWeight.w500,
      letterSpacing: 0.1,
      decoration: TextDecoration.none);

  static TextStyle body1() => TextStyle(
      fontSize: 1.955.fz,
      color: ColorHelper.materialColor.shade900,
      fontWeight: FontWeight.w400,
      letterSpacing: 0.5,
      decoration: TextDecoration.none);

  static TextStyle body2() => TextStyle(
      fontSize: 1.711.fz,
      color: ColorHelper.materialColor.shade900,
      fontWeight: FontWeight.w500,
      letterSpacing: 0.25,
      decoration: TextDecoration.none);

  static TextStyle button() => TextStyle(
      fontSize: 1.711.fz,
      color: ColorHelper.accentColor,
      fontWeight: FontWeight.w500,
      letterSpacing: 1.25,
      decoration: TextDecoration.none);

  static TextStyle caption() => TextStyle(
      fontSize: 1.222.fz,
      color: ColorHelper.materialColor.shade900,
      fontWeight: FontWeight.w400,
      letterSpacing: 1.5,
      decoration: TextDecoration.none);

  static TextStyle appBarTextStyle() =>
      TextStyle(color: Colors.white, fontSize: 2.5.fz);

  static TextStyle sliverAppBarTextStyle() => TextStyle(color: Colors.white);
}
