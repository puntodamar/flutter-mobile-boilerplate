import 'package:boilerplate/screens/example/firebase/audio_streaming_screen.dart';
import 'package:boilerplate/screens/example/firebase/file_downloader_screen.dart';
import 'package:boilerplate/screens/example/firebase/firebase_auth_screen.dart';
import 'package:boilerplate/screens/example/firebase/firebase_database_screen.dart';
import 'package:boilerplate/screens/example/firebase/firebase_storage_screen.dart';
import 'package:boilerplate/screens/example/screens/adaptive_single_screen_content.dart';
import 'package:boilerplate/screens/example/screens/auto_imply_leading_screen.dart';
import 'package:boilerplate/screens/example/screens/force_android_drawer_screen.dart';
import 'package:boilerplate/screens/example/screens/notification_screen.dart';
import 'package:boilerplate/screens/example/screens/without_boilerplate_screen.dart';
import 'package:boilerplate/screens/example/typography_screen.dart';
import 'package:boilerplate/screens/expansion_tile_sample.dart';
import 'package:boilerplate/screens/hero_screen.dart';
import 'package:boilerplate/screens/example/screens/adaptive_bottom_navbar_screen.dart';
import 'package:boilerplate/screens/example/screens/adaptive_single_screen.dart';
import 'package:boilerplate/screens/example/screens/android_top_navbar_screen.dart';
import 'package:boilerplate/screens/example/screens/form_field_screen.dart';
import 'package:boilerplate/screens/example/screens/radio_button_screen.dart';
import 'package:boilerplate/screens/example/screens/slider_screen.dart';
import 'package:boilerplate/widgets/navigation_drawer/nav_bar_list_item.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';
import 'color_helper.dart';
import 'extensions.dart';

class DrawerEntry {
  final String title;
  final List<dynamic> items;
  final List<dynamic> children;
  final Widget leading;
  final int index;
  final bool isRoot;
  int parentIndex;


  DrawerEntry(
      {@required this.title,
      @required this.index,
      this.parentIndex,
      this.isRoot = false,
      this.children = const <DrawerEntry>[],
      this.items,
      this.leading});
}

class ExpansionTileParentData {
  final GlobalKey<AppExpansionTileState> key;
  final int index;
  int parentIndex;

  ExpansionTileParentData({@required this.key, @required this.index, this.parentIndex});
}

class NavigationHelper {
  static String currentRouteName;
  static GlobalKey navigationKey;

  static  ItemScrollController itemScrollController =
      ItemScrollController();
  static ItemPositionsListener _itemPositionsListener =
      ItemPositionsListener.create();
  static int navDrawerIndex = 0;
  static bool scrollToSelected = false;

  static final List<Tab> topAppBarTabItems = [
    new Tab(
      icon: new Icon(Icons.email),
      text: "Email",
    ),
    new Tab(
      icon: new Icon(Icons.queue_music),
      text: "Music",
    ),
    new Tab(
      icon: new Icon(Icons.shopping_cart),
      text: "Shop",
    ),
  ];

  static final List<Widget> topAppBarTabScreens = [
    Text('Top App 1').align(),
    Text('Top App 2').align(),
    Text('Top App 3').align(),
  ];

  static List<Widget> bottomTabScreens() {
    return [
      AdaptiveSingleScreenHelper("Tab 1"),
      AdaptiveSingleScreenHelper("Tab 2"),
      AdaptiveSingleScreenHelper("Tab 3"),
    ];
  }

  static List<BottomNavigationBarItem> bottomNavBarItems() {
    TextStyle textStyle = TextStyle(
      fontSize: 2.fz,
    );
    return [
      BottomNavigationBarItem(
        icon: const Icon(Icons.home),
          label: 'Tab 1',
      ),
      BottomNavigationBarItem(
        icon: const Icon(Icons.assignment),
        label: 'Tab 2',
      ),
      BottomNavigationBarItem(
        icon: const Icon(Icons.notifications),
        label: 'Tab 3',
      ),
    ];
  }

  static FloatingActionButton floatingActionButton() {
    return FloatingActionButton(
      backgroundColor: ColorHelper.materialColor.shade500,
      onPressed: () => print("FAB pressed"),
      tooltip: 'Increment',
      child: new Icon(Icons.add, color: ColorHelper.accentColor,),
    );
  }

  static List<dynamic> drawerEntries() {
    return [
      DrawerEntry(
          index: 0,
          isRoot: true,
          title: "Screens",
          leading: Icons.smartphone.asIcon(),
          items: [
            DrawerEntry(
                title: "Sub Navigation 1",
                index: 1,
                parentIndex: 0,
                leading: Icons.save.asIcon(),
                items: [
                  NavBarListItem(
                    index: 2,
                    navigationKey: navigationKey,
                    title: "Adaptive Single screen",
                    routeDestination: AdaptiveSingleScreen.routeName,
                  )
                ]),
            DrawerEntry(
                title: "Sub Navigation 2",
                index: 3,
                parentIndex: 0,
                leading: Icons.supervised_user_circle.asIcon(),
                items: [
                  NavBarListItem(
                    index: 4,
                    navigationKey: navigationKey,
                    title: "Dont tap this",
                    routeDestination: "/child",
                  )
                ]),
            NavBarListItem(
              index: 5,
              navigationKey: navigationKey,
              title: 'Dont tap this',
              routeDestination: "wawa",
            ),
            NavBarListItem(
              index: 6,
              navigationKey: navigationKey,
              title: "Adaptive Bottom Navbar Screen",
              routeDestination: AdaptiveBottomNavbarScreen.routeName,
            ),
//      Divider(thickness: 8,),
            NavBarListItem(
              index: 7,
              navigationKey: navigationKey,
              title: "Android Top Navbar Screen",
              routeDestination: AndroidTopNavbarScreen.routeName,
            ),
            NavBarListItem(
              index: 8,
              navigationKey: navigationKey,
              title: "Form field screen",
              routeDestination: FormFieldScreen.routeName,
            ),
            NavBarListItem(
              index: 9,
              navigationKey: navigationKey,
              title: "Auto Imply Leading Screen",
              routeDestination: AutoImplyLeadingScreen.routeName,
            ),
            NavBarListItem(
              index: 10,
              navigationKey: navigationKey,
              title: "Force Android Drawer Screen",
              routeDestination: ForceAndroidDrawerScreen.routeName,
            ),
            NavBarListItem(
              index: 11,
              navigationKey: navigationKey,
              title: "Hero",
              routeDestination: HeroScreen.routeName,
            ),
            NavBarListItem(
              index: 12,
              navigationKey: navigationKey,
              title: "Without Boilerplate",
              routeDestination: WithoutBoilerplateScreen.routeName,
            ),
          ]),
      DrawerEntry(
        isRoot: true,
        index: 13,
        title: "Text & Typography",
        leading: Icons.text_fields.asIcon(),
        items: [
          NavBarListItem(
            index: 14,
            navigationKey: navigationKey,
            title: "Typography",
            routeDestination: TypoGraphyScreen.routeName,
          ),
        ]
      ),
      DrawerEntry(
        isRoot: true,
        index: 15,
          title: "Form",
          leading: Icons.list.asIcon(),
          items: [
        NavBarListItem(
          index: 16,
          navigationKey: navigationKey,
          title: "Slider",
          routeDestination: SliderScreen.routeName,
        ),
        NavBarListItem(
          index: 17,
          navigationKey: navigationKey,
          title: "Radio Button",
          routeDestination: RadioButtonScreen.routeName,
        ),
      ]),
      DrawerEntry(
          isRoot: true,
          index: 18,
          title: "Firebase",
          leading: Icons.android.asIcon(),
          items: [
            NavBarListItem(
              index: 19,
              navigationKey: navigationKey,
              title: "Profile",
              routeDestination: FirebaseProfileScreen.routeName,
            ),
            NavBarListItem(
              index: 20,
              navigationKey: navigationKey,
              title: "Database",
              routeDestination: FirebaseDatabaseScreen.routeName,
            ),
            NavBarListItem(
              index: 21,
              navigationKey: navigationKey,
              title: "Audio Stream",
              routeDestination: AudioStreamingScreen.routeName,
            ),
            NavBarListItem(
              index: 21,
              navigationKey: navigationKey,
              title: "Storage",
              routeDestination: FirebaseStorageScreen.routeName,
            ),
            NavBarListItem(
              index: 22,
              navigationKey: navigationKey,
              title: "File Downloader",
              routeDestination: FileDownloaderScreen.routeName,
            ),
            NavBarListItem(
              index: 23,
              navigationKey: navigationKey,
              title: "Notification",
              routeDestination: FileDownloaderScreen.routeName,
            )
          ]),
      NavBarListItem(
        index: 24,
        navigationKey: navigationKey,
        title: "Notification",
        routeDestination: NotificationScreen.routeName,
      )
    ];
  }
}
