import 'package:boilerplate/helpers/color_helper.dart';
import 'package:boilerplate/widgets/menu_area.dart';
import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'device_info_helper.dart';
import 'dart:io' show Platform;
import 'package:flutter_screenutil/flutter_screenutil.dart';


extension ResponsiveUI on num {
//  double get scl => this * 1.sw;
  double get scl => DeviceInfoHelper.scalingFactor * this;
  double get fz => DeviceInfoHelper.scalingFactor * this;

}

extension DateFormatter on DateTime {
  get ddMmYyyy => DateFormat.yMMMd().format(this).toString();

  get hhmm => DateFormat.Hm().format(this).toString();
}

extension IconExtension on IconData {
  Icon asIcon([bool useAccentColor = true]) => Icon(this, color: useAccentColor ? ColorHelper.iconAccentThemeData().color : ColorHelper.iconThemeData().color,);
}

extension WidgetExtension on Widget {

  Widget scrollable([bool scrollable = true]) => SingleChildScrollView(child: this, physics: scrollable ? AlwaysScrollableScrollPhysics() : NeverScrollableScrollPhysics(),);

  Container addContainer() => Container(child: this);

  Widget expanded() => Expanded(child: this,);

  Widget flexible() => Flexible(child: this,);

  Widget forceAsMaterialWidget({Color backgroundColor = Colors.white}) => Material(child: this, color: backgroundColor,);

  Widget align([AlignmentGeometry align = Alignment.center]) => Align(
        alignment: align,
        child: this,
      );

  Widget boxed({double height = 0.0, double width = 0.0}) => SizedBox(
        height: height,
        width: width,
        child: this,
      );

  // **********
  // PADDING
  // **********
  Widget uiPaddingAll([double padding = 2.0]) =>
      Padding(padding: EdgeInsets.all(padding.w), child: this);

  Widget uiPaddingOnly(
          {double l = 0.0, double t = 0.0, double r = 0.0, double b = 0.0}) =>
      Padding(
          padding: EdgeInsets.only(
              left: l.w, top: t.w, right: r.w, bottom: b.w),
          child: this);

  Widget uiPaddingSymmetric({double v = 0.0, double h = 0.0}) => Padding(
      padding: EdgeInsets.symmetric(vertical: v.w, horizontal: h.w),
      child: this);

  // **********
  // THEME
  // **********
  Widget formInputWrapTheme() => Theme(
    data: ThemeData(
      unselectedWidgetColor: ColorHelper.materialColor.shade100
    ),
    child: this,
  );


  Widget center() => Center(child: this,);
}

extension AdaptiveTheme on BuildContext {
  get themeData =>
      Platform.isAndroid ? Theme.of(this) : CupertinoTheme.of(this);
}
