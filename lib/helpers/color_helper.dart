import 'package:boilerplate/helpers/typography_helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'extensions.dart';

class ColorHelper {
  static MaterialColor materialColor;
  static Color platformAppBarBackgroundColor;
  static TextStyle defaultTextStyle;
  static Color accentColor;
  static TextStyle helperStyle;

  static init() {
    platformAppBarBackgroundColor = materialColor.shade500;
    defaultTextStyle = TextStyle(
        color: materialColor.shade500, decoration: TextDecoration.none);
    accentColor = materialColor.shade50;
  }

  static void setMaterialColor(MaterialColor materialColor) {
    ColorHelper.materialColor = materialColor;
    init();
  }

  static Color outlineButtonMaterialState(Set<MaterialState> states) {
    const Set<MaterialState> interactiveStates = <MaterialState>{
      // MaterialState.pressed,
      MaterialState.hovered,
      MaterialState.focused,
      MaterialState.disabled,
    };
    return states.any(interactiveStates.contains)
        ? materialColor.shade100
        : materialColor.shade500;
  }

  static TextStyle _materialStateText(Set<MaterialState> states) {
    return TextStyle(color: Colors.red);
  }

  static OutlinedBorder _outlineButtonBorderState(
      Set states) {
    return RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(18.0),
        side: BorderSide(
          color: Colors.red,
          style: BorderStyle.solid,
          width: 2.5,
        )
    );
  }

  static ThemeData materialThemeData() {

    return ThemeData(
      primarySwatch: materialColor,
      // accentColor: Colors.red,
      // change :
      // circular progress
      // accentColor: ,
      colorScheme: ColorScheme.fromSwatch(
        primarySwatch: materialColor,
        backgroundColor: Colors.white,
        cardColor: Colors.white,
        errorColor: Colors.red,
        brightness: Brightness.light,
        accentColor: Colors.orangeAccent,
        // primaryColorDark: darkColorSwatch.shade500,
      ),
      textTheme:
      TextTheme(bodyText2: TextStyle(color: materialColor.shade500)),
      buttonTheme: ButtonThemeData(
        buttonColor: materialColor.shade500,
        disabledColor: materialColor.shade200,
        focusColor: Colors.red,
        highlightColor: Colors.red,
        hoverColor: Colors.red,
        textTheme: ButtonTextTheme.primary,
      ),
      unselectedWidgetColor: materialColor.shade100,
      inputDecorationTheme: InputDecorationTheme(
        // DEFAULT BORDER
        border: OutlineInputBorder(
            borderSide: BorderSide(
                color: materialColor.shade900
            )
        ),
        helperStyle: TextStyle(color: materialColor.shade500, fontStyle: FontStyle.italic),
        hintStyle: TextStyle(color: materialColor.shade100),
        focusedBorder: UnderlineInputBorder(
            borderSide:
            BorderSide(color: materialColor.shade900, width: 2)
        ),
        errorBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.red, width: 2)
        ),
        errorStyle: TextStyle(fontStyle: FontStyle.italic, color: Colors.red),
        labelStyle: TextStyle(),
        prefixStyle: TextStyle(),
        suffixStyle: TextStyle(),
      ),
      sliderTheme: SliderThemeData(
        valueIndicatorColor: materialColor.shade500,
        valueIndicatorTextStyle: TextStyle(color: materialColor.shade50),
        activeTickMarkColor: Colors.white,
        thumbColor: materialColor.shade500,
        overlayColor: materialColor.shade50,
        activeTrackColor: materialColor.shade500,
        inactiveTrackColor: materialColor.shade50,
        inactiveTickMarkColor: materialColor.shade500,
      ),


      // textTheme:
      // primarySwatch: materialColorSwatch,
      brightness: Brightness.light,
      visualDensity: VisualDensity.adaptivePlatformDensity,
    );
  }

  static CupertinoThemeData cupertinoThemeData() {
    return CupertinoThemeData(
          brightness: Brightness.dark,
          scaffoldBackgroundColor: Colors.white,
          primaryColor: materialColor.shade500,
          // affect action color
          barBackgroundColor: materialColor.shade900,
          textTheme: CupertinoTextThemeData(
              textStyle: TextStyle(color: materialColor.shade500),
              navTitleTextStyle: TextStyle(
                  color: materialColor.shade50,
                  fontWeight: FontWeight.bold,
                  fontSize: 16)),
          primaryContrastingColor: Colors.white // for button color
          );
  }

  static IconThemeData iconThemeData() =>
      IconThemeData(color: materialColor.shade500);

  static IconThemeData iconAccentThemeData() =>
      IconThemeData(color: accentColor);

  static MaterialNavBarData materialNavBarData() =>
      MaterialNavBarData(
          backgroundColor: materialColor.shade500,
          selectedItemColor: accentColor,
          unselectedItemColor: Colors.black);

  static TextStyle appBarTextStyle() =>
      TextStyle(color: Colors.white, fontSize: 2.fz);

  static TextStyle sliverAppBarTextStyle() => TextStyle(color: Colors.white);

  static MaterialRaisedButtonData materialRaisedButtonData([Color color]) =>
      MaterialRaisedButtonData(color: color ?? materialColor.shade500,);

  static MaterialFlatButtonData materialFlatButtonData([Color color]) =>
      MaterialFlatButtonData(
        color: color ?? materialColor.shade500,
        textColor: Colors.white,
      );

  static CupertinoFilledButtonData cupertinoFilledButtonData([Color color]) =>
      CupertinoFilledButtonData();

  static CupertinoButtonData cupertinoButtonData([Color color]) =>
      CupertinoButtonData(color: color ?? materialColor.shade500);

  static MaterialSwitchData materialSwitchData() =>
      MaterialSwitchData(
          activeColor: platformAppBarBackgroundColor,
          inactiveThumbColor: accentColor
      );

  static CupertinoSwitchData cupertinoSwitchData() =>
      CupertinoSwitchData(
        activeColor: platformAppBarBackgroundColor,
      );

  static MaterialSliderData materialSliderData() =>
      MaterialSliderData(
        activeColor: materialColor.shade500,
        inactiveColor: materialColor.shade100,


      );

  static CupertinoSliderData cupertinoSliderData() =>
      CupertinoSliderData(
        activeColor: materialColor.shade500,
      );

  static MaterialProgressIndicatorData materialProgressIndicatorData() =>
      MaterialProgressIndicatorData(
          backgroundColor: materialColor.shade200,
          valueColor: new AlwaysStoppedAnimation<Color>(materialColor.shade500)
      );

  static CupertinoProgressIndicatorData cupertinoProgressIndicatorData() =>
      CupertinoProgressIndicatorData(
          radius: 40.w
      );

}
