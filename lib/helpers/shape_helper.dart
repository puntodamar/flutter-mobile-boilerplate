import 'package:flutter/material.dart';

import 'color_helper.dart';

enum TextInputShape{
  rectangle, rounded
}

class ShapeHelper {
  static final ShapeBorder buttonRoundedRectangle = RoundedRectangleBorder(
    borderRadius: BorderRadius.circular(18.0),
  );

  static final OutlineInputBorder textFieldRectangle = OutlineInputBorder(
      borderSide: BorderSide(
          color: ColorHelper.materialColor.shade900
      )
  );

  static final OutlineInputBorder textFieldErrorRectangle = OutlineInputBorder(
      borderSide: BorderSide(
          color: Colors.red
      )
  );

  static final OutlineInputBorder textFieldErrorRounded = OutlineInputBorder(
      borderRadius: BorderRadius.circular(25),
      borderSide: BorderSide(
          color: Colors.red
      )
  );

  static final OutlineInputBorder textFieldRounded = OutlineInputBorder(
      borderRadius: BorderRadius.circular(25),
      borderSide: BorderSide(
          color: ColorHelper.materialColor.shade900
      )
  );
}
