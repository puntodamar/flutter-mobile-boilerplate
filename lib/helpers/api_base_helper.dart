import 'dart:io';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'app_exception.dart';
import 'constants.dart' as constants;

 class ApiBaseHelper {

   static Map<String, String> defaultHeaders =  {
   'Content-Type': 'application/json; charset=UTF-8'
   };

  static Future<dynamic> get(String url) async {
    var responseJson;
    try {

      final response = await http.get(url);
      responseJson = _returnResponse(response);
    } on SocketException {
      throw FetchDataException('No Internet connection');
    }
    return responseJson;
  }

  static Future<dynamic> post(String url, Map<String, Object> body, {Map<String, String> headers}) async {
    var responseJson;
    try {
      headers != null ? headers = {...headers, ...defaultHeaders} : headers = defaultHeaders;
      print(constants.baseUrl + url);
      final response = await http.post(url, body: json.encode(body), headers: headers);
      responseJson = _returnResponse(response);
    } on SocketException {
      throw FetchDataException('No Internet connection');
    }
    return responseJson;
  }

  static dynamic _returnResponse(http.Response response) {
    var body = json.decode(response.body);
    print(body);
    if([201, 200, 500, 401, 422].contains(response.statusCode) || body['data'] != null)
      return body;
//    else if(body['error'] != null)
//      throw Exception(body['error']['errors'][0]);
    else
      throw Exception(body);
  }
}