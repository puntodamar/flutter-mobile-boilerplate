import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:boilerplate/helpers/color_helper.dart';
import 'package:boilerplate/helpers/typography_helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:get/get.dart';
import 'package:recase/recase.dart';
import 'dart:io' show Platform;
import 'extensions.dart';

class DialogHelper {
  static dynamic showDialogIfError(Map<String, dynamic> response) async {
    if (response.containsKey('error') || response.containsKey('errors')) {
      switch (response['error']['code']) {
        case 500:
          showBottomSheetErrorDialog(response['error']['errors'][0]);
          break;
        default:
          showBottomSheetErrorDialog(response['error']['errors'][0]);
      }
    } else
      return response;
  }

  static Future<bool> showAlertDialog(
      {String title = "Alert Dialog Title",
      Widget content = const Text('Hello!'),
      List<Widget> actions,
      bool dismissible = true,
        dynamic then,
      Icon suffixIcon,
      Icon prefixIcon}) async {
    return await showPlatformDialog(
      context: Get.context,
      barrierDismissible: dismissible, // user must tap button!
      builder: (BuildContext ctx) {
        return PlatformAlertDialog(
          title: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              if (prefixIcon != null) prefixIcon.paddingOnly(right: 10),
              Text(title.titleCase, style: TypographyHelper.headline6(),).center(),
              if (suffixIcon != null) suffixIcon
            ],
          ),
          content: content,
          actions: actions,
        );
      },
    );
  }

  static showActionSheet(
      {BuildContext context,
      String title = "Action sheet title",
      String message = "Action sheet Message",
      List<Widget> actions,
      Widget cancelButton}) {
    if (Platform.isIOS)

      showCupertinoModalPopup(
          context: context,
          builder: (_) {
            return CupertinoActionSheet(
              title: Text(
                title.titleCase,
                style: TextStyle(
                    color: ColorHelper.materialColor.shade500,
                    fontWeight: FontWeight.bold,
                    fontSize: 2.fz),
              ),
              message: Text(
                message,
                style: TextStyle(color: ColorHelper.materialColor.shade900),
              ),
              actions: actions,
                cancelButton: CupertinoActionSheetAction(
                  child: Text('Cancel', style: TextStyle(color: Colors.red),),
                  onPressed: () => Navigator.of(Get.context).pop(),
                )
              // cancelButton: cancelButton,
            );
          });
    else
      return ListTile();
  }

  static showModalSheet(Widget child, [int duration = 2]) async {
    Widget content = Container(
      width: double.infinity,
      height: 0.1.sh,
      decoration: BoxDecoration(
        color: ColorHelper.accentColor,
      ),
      child: child.uiPaddingSymmetric(v: 20, h: 20),
    );
    Get.bottomSheet(
        PlatformWidget(
            material: (_, __) => content, cupertino: (_, __) => content),
        isDismissible: false);
    await Future.delayed(Duration(seconds: duration));
    Navigator.of(Get.context).pop();

  }

  static showBottomSheetErrorDialog(String message) async {
      Widget content = Container(
        width: double.infinity,
        height: 0.9.sh,
        decoration: BoxDecoration(
          color: ColorHelper.materialColor.shade900,
        ),
//                  borderRadius: BorderRadius.circular(10)),x\
//        color: Colors.red,
        child: SingleChildScrollView(
          child: Text(message,
              style:
              TextStyle(color: ColorHelper.accentColor)),
        ).paddingAll(20),
      );

      Get.bottomSheet(
          PlatformWidget(
              material: (_, __) => content, cupertino: (_, __) => content),
          isDismissible: false);

    await Future.delayed(Duration(seconds: 5));
    Get.back();
    await Future.delayed(Duration(seconds: 1));
  }
}
