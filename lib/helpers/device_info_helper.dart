import 'package:flutter/material.dart';

class DeviceInfoHelper {
  static MediaQueryData _mediaQueryData;
  static double screenWidth;
  static double screenHeight;
  static double blockSizeHorizontal;
  static double blockSizeVertical;

  static double _safeAreaHorizontal;
  static double _safeAreaVertical;
  static double safeBlockHorizontal;
  static double safeBlockVertical;

  static bool get isTablet {
    if (_mediaQueryData.orientation == Orientation.portrait ){
      return screenWidth > 600;
    }
    else
      return screenHeight > 600;
  }

  static double get scalingFactor {
    if(_mediaQueryData == null){
      return 1.0;
    }
    else return _mediaQueryData.orientation == Orientation.portrait ? safeBlockVertical : safeBlockHorizontal;
  }

  static Orientation get orientation {
    return _mediaQueryData.orientation;
  }

  static bool get isLandscape {
    return _mediaQueryData.orientation == Orientation.landscape;
  }

  static bool get isPortrait {
    return _mediaQueryData.orientation == Orientation.portrait;
  }

  static void init(BuildContext context) {
    _mediaQueryData = MediaQuery.of(context);
    screenWidth = _mediaQueryData.size.width;
    screenHeight = _mediaQueryData.size.height;
    blockSizeHorizontal = screenWidth / 100;
    blockSizeVertical = screenHeight / 100;

    _safeAreaHorizontal = _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVertical = _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;
    safeBlockHorizontal = (screenWidth - _safeAreaHorizontal) / 100;
    safeBlockVertical = (screenHeight - _safeAreaVertical) / 100;
  }
}
