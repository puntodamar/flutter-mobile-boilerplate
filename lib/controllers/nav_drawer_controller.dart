import 'package:boilerplate/helpers/navigation_helper.dart' show ExpansionTileParentData;
import 'package:boilerplate/widgets/navigation_drawer/adaptive_drawer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class NavDrawerController extends GetxController {
  int _listIndex;
  var selectedParentIndex = 0.obs;
  var currentRouteName = "".obs;
  bool isOpened = false;
  var expansionTileParents = [].obs;
  List<Widget> tabScreens = List<Widget>();
  List<BottomNavigationBarItem> bottomNavigationBarItems = List<BottomNavigationBarItem>();
  List<Tab> tabs = List<Tab>();

  GlobalKey<AdaptiveDrawerState> navKey;

  get activeIndex => _listIndex;

  void setNavigationKey(key) => navKey = key;

  // ignore: invalid_use_of_protected_member
  void addKey(ExpansionTileParentData parentKey) => expansionTileParents.value.add(parentKey);

  void toggle(){
    isOpened = !isOpened;
    update();
  }
  
  void updateRoute(String newRouteName) {
    currentRouteName.value = newRouteName;
    update();
  }
  
  void updateParentIndex(int newParentIndex, [bool notifyListener = false]){
    print('upcate parent called -- ${notifyListener}');
    selectedParentIndex.value = newParentIndex;
    if(notifyListener) update();
  }

  void updateNavigation(int listIndex, String currentRouteName) {
    _listIndex = listIndex;

    this.currentRouteName.value = currentRouteName;
    // update();
    // Get.to(page);
    Get.offAllNamed(currentRouteName);

  }
}