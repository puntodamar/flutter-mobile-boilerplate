import 'dart:typed_data';

import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'package:multi_image_picker/multi_image_picker.dart';

class ImagePickerController extends GetxController {

  List<Asset> images = List<Asset>();
  Uint8List imageAsBytes;

  String _error;
  final _picker = ImagePicker();

  Future <void> getImage(ImageSource imageSource) async {
    final pickedFile = await _picker.getImage(source: imageSource);
    if (pickedFile != null) {
      imageAsBytes = await pickedFile.readAsBytes();
      print(imageAsBytes);
      update();
    } else {
      print('No image selected.');
    }
  }

  Future<void> getImages(
      ImageSource imageSource, {
        int maxImages = 1,
      }) async {
    try{
      images = await MultiImagePicker.pickImages(
          maxImages: maxImages,
          enableCamera: true);
    }
    on Exception catch (e) {
      _error = e.toString();
    }
  }

}
