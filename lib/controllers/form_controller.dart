import 'package:get/get.dart';

class FormController extends GetxController {
  bool isEditing = false;
  bool isKeyboardOpen = false;

  void toggleIsEditing() {
    isEditing = !isEditing;
    update();

  }

  void setKeyboard(bool open){
    isKeyboardOpen = open;
    update();
  }

}