import 'dart:convert';

import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import 'firebase_controller.dart';

class AuthController extends GetxController {
  String _accessToken;
  String _refreshToken;
  DateTime _expiryDate;
  bool _isSubmitting = false;

  bool get isAuthenticated => _accessToken != null;
  String get refreshToken => _refreshToken;
  bool get isSubmitting => _isSubmitting;

  void submitFinished(){
    _isSubmitting = false;
    update();
  }

  void toggleSubmit(){
    _isSubmitting = !_isSubmitting;
    update();
  }

  String get token {
    if (_expiryDate != null &&
        _expiryDate.isAfter(DateTime.now()) &&
        _accessToken != null) {
      return _accessToken;
    }
    return _accessToken;
  }

  Future<dynamic> tryAutoLogin() async {
    print("try auto logian");
    try {
      FirebaseController firebaseController = Get.find();
      _accessToken = await firebaseController.isAuthUserExists();

      // GetStorage storage = GetStorage();
      // print("aaa");
      // print(json.decode(storage.read('userDataa')) ?? null);
      // final tokenData = json.decode(storage.read('userDataa')) ?? null;
      // print("bbb");
      // print(tokenData);
      // if(tokenData != null){
      //       _accessToken = tokenData['access_token'];
      //       _refreshToken = tokenData['refresh_token'];
      //       // update();
      // }
      // else{
      //   print("else");
      //   FirebaseController firebaseController = Get.find();
      //   _accessToken = firebaseController.isAuthUserExists();
      // }
      // print("finish");
      // firebaseController.
    }
    on Exception catch(e){
      print(e);
    }
  }
}