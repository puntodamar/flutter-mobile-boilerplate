import 'dart:async';

import 'package:boilerplate/models/random_string_model.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class RandomStringListDatabaseListener extends GetxController {
  StreamSubscription<Event> _onCreateSubscription;
  StreamSubscription<Event> _onChangedSubscription;
  StreamSubscription<Event> _onDeleteSubscription;

  var list = List<RandomStringModel>().obs;
  GlobalKey<AnimatedListState> listKey;

  @override
  void onClose() {
    _onCreateSubscription.cancel();
    _onChangedSubscription.cancel();
    _onDeleteSubscription.cancel();
    super.onClose();
  }

  void setListKey(GlobalKey<AnimatedListState> key) {
    this.listKey = key;
    update();
  }

  @override
  void onInit() {
    var doc = FirebaseDatabase.instance.reference().child("random-string-list");

    _onCreateSubscription = doc.onChildAdded.listen((event) {
      if (listKey != null && listKey.currentState != null) {
        listKey.currentState.insertItem(list.length,
            duration: const Duration(milliseconds: 500));
      }
      list.add(new RandomStringModel.fromSnapshot(event.snapshot));
      update();
    });

    _onDeleteSubscription = doc.onChildRemoved.listen((event) {
      if (listKey != null && listKey.currentState != null){
        int removedIndex = list.indexWhere((element) => element.key == event.snapshot.key);
        listKey.currentState.removeItem(removedIndex, (_, animation) => Container(),
            duration: const Duration(milliseconds: 500));
        list.removeAt(removedIndex);
        update();
      }

    });
    super.onInit();
  }
}
