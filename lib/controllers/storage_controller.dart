import 'dart:io';

import 'package:boilerplate/controllers/http_helper.dart';
import 'package:dio/dio.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:get/get.dart';
import 'package:path_provider/path_provider.dart';

class StorageController extends GetxController {

  FirebaseStorage firebaseStorage;
  var progress = <double>[].obs;

  Future<void> firebaseDownload(StorageReference ref) async {
    final String url = await ref.getDownloadURL();

    final FileDownloadResponse downloadResponse = await HttpHelper.download(progress.length + 1, url, saveToTemp: true);

    final StorageFileDownloadTask task = ref.writeToFile(downloadResponse.file);
    final int byteCount = (await task.future).totalByteCount;
    final String tempFileContents = await downloadResponse.file.readAsString();

    final String fileContents = downloadResponse.response.data;
    final String name = await ref.getName();
    final String bucket = await ref.getBucket();
    final String path = await ref.getPath();

    print('Success!\n Downloaded $name \n from url: $url @ bucket: $bucket\n '
        'at path: $path \n\nFile contents: "$fileContents" \n'
        'Wrote "$tempFileContents" to tmp.mp3');
  }

  Future<String> downloadAndSaveFile(String url, String fileName) async {
    final Directory directory = await getExternalStorageDirectory();
    final String filePath = '${directory.path}/$fileName';
    Response<List<int>> response = await Dio().get<List<int>>(url,
      options: Options(responseType: ResponseType.bytes), // // set responseType to `bytes`
    );
    final File file = File(filePath);
    await file.writeAsBytes(response.data);
    return filePath;
  }

}