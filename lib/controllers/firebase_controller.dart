import 'dart:async';

import 'package:boilerplate/helpers/dialog_helper.dart';
import 'package:boilerplate/helpers/typography_helper.dart';
import 'package:boilerplate/screens/example/screens/adaptive_single_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:boilerplate/helpers/extensions.dart';
import 'package:get_storage/get_storage.dart';
import 'package:google_sign_in/google_sign_in.dart';

enum LoginType { Basic, Google, Facebook, Twitter, Github }

class FirebaseController extends GetxController {
  bool isWaitingResponse = false;
  User currentUser;
  String _token;
  String displayName;

  Future<String> isAuthUserExists() async {
    GetStorage box = GetStorage();
    _token = box.read("firebase-token");
    if (_token == null) {
      try {
        currentUser = FirebaseAuth.instance.currentUser;
        displayName = currentUser.displayName.length > 0
            ? currentUser.displayName
            : currentUser.email;
        _token = await currentUser.getIdToken(true);
        GetStorage box = GetStorage();
        box.write("firebase-token", _token);
        return _token;
      } on Exception catch (e) {
        return null;
      }
    } else
      return _token;
  }

  void toggleSubmit(){
    isWaitingResponse = !isWaitingResponse;
    update();
  }

  Future<UserCredential> signInWithGoogle() async {
    // Trigger the authentication flow
    final GoogleSignInAccount googleUser = await GoogleSignIn().signIn();

    // Obtain the auth details from the request
    final GoogleSignInAuthentication googleAuth =
        await googleUser.authentication;

    // Create a new credential
    final GoogleAuthCredential credential = GoogleAuthProvider.credential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );

    // Once signed in, return the UserCredential
    return await FirebaseAuth.instance.signInWithCredential(credential);
  }

  void login(LoginType loginType,
      {String email, String password, bool popDialog}) async {
    toggleSubmit();
    try {
      String token;
      UserCredential userCredential;
      if (loginType == LoginType.Basic) {
        userCredential = await FirebaseAuth.instance
            .signInWithEmailAndPassword(email: email, password: password);
      }
      if (loginType == LoginType.Google) {
        userCredential = await signInWithGoogle();
      }
      token = await userCredential.user.getIdToken();
      GetStorage box = GetStorage();
      box.write("firebase-token", token);

      Get.offAndToNamed(AdaptiveSingleScreen.routeName);
    } on FirebaseAuthException catch (e) {
      String message;
      if (e.code == 'user-not-found' || e.code == 'wrong-password') {
        message = "Invalid username/password";
      } else if (e.code == "too-many-requests")
        message = "Too much requests. Please wait a few minutes";

      DialogHelper.showAlertDialog(
          title: "Error!",
          prefixIcon: Icons.warning.asIcon(false),
          content: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                child: Text(
                  message,
                  style: TypographyHelper.body1(),
                  textAlign: TextAlign.center,
                ),
              )
            ],
          ));
    }
    toggleSubmit();
  }

  void updateUser(Map<String, Object> data) {
    toggleSubmit();
    try {
      data.forEach((key, value) {
        if (key == "displayName") {
          FirebaseAuth.instance.currentUser.updateProfile(displayName: value);
        }
        // if(key == "phone")
        //   FirebaseAuth.instance.currentUser.updatePhoneNumber()
      });
    } on Exception catch (e) {
      print("update exception");
    }

    toggleSubmit();
  }

  void register(String email, String password) async {
    toggleSubmit();
    try {
      UserCredential userCredential = await FirebaseAuth.instance
          .createUserWithEmailAndPassword(email: email, password: password);

      GetStorage getStorage = GetStorage();
      getStorage.write(
          "firebase-token", await userCredential.user.getIdToken());

      AlertDialog(
        content: Text(
          'Your account is successfully created!',
          style: TypographyHelper.body1().copyWith(color: Colors.green),
        ),
        actions: [
          FlatButton(
            child: Text('OK'),
            onPressed: () {
              Navigator.of(Get.context).pop();
              Get.offAndToNamed(AdaptiveSingleScreen.routeName);
            },
          )
        ],
      );
    } on FirebaseAuthException catch (e) {
      String message;
      if (e.code == 'weak-password') {
        message = "Your password is too weak";
      } else if (e.code == 'email-already-in-use') {
        message = "Email is already taken";
      }

      DialogHelper.showAlertDialog(
          title: "Error!",
          prefixIcon: Icons.warning.asIcon(false),
          content: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                message,
                style: TypographyHelper.body1(),
              )
            ],
          ));
    } catch (e) {
      DialogHelper.showAlertDialog(
          title: "Error!",
          prefixIcon: Icons.warning.asIcon(false),
          content: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                e.toString(),
                style: TypographyHelper.body1(),
              )
            ],
          ));
    }
    toggleSubmit();
  }
}
