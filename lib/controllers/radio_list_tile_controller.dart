import 'package:get/get.dart';

class RadioListTileController extends GetxController {
  dynamic _value;

  get value => _value;

  void setValue(dynamic value){
    _value = value;
    update();
  }
}