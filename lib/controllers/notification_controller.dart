import 'dart:typed_data';

import 'package:boilerplate/helpers/color_helper.dart';
import 'package:boilerplate/screens/example/screens/notification_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get/get.dart';
import 'package:rxdart/subjects.dart';
import 'package:timezone/data/latest.dart' as tz;
import 'package:timezone/timezone.dart' as tz;

class ReceivedNotification {
  ReceivedNotification({
    @required this.id,
    @required this.title,
    @required this.body,
    @required this.payload,
  });

  final int id;
  final String title;
  final String body;
  final String payload;
}

class NotificationChannel {
  final String id;
  final String name;
  final String description;

  NotificationChannel(this.id, this.name, this.description);
}

class NotificationController extends GetxController {
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

  final NotificationChannel basicChannel =
      NotificationChannel("1", "Basic Notification", "description1");
  final NotificationChannel customSoundChannel =
      NotificationChannel("2", "Custom Sound Channel", "description2");
  final NotificationChannel customVibrationChannel =
      NotificationChannel("3", "Custom Vibration Channel", "description3");

  final NotificationChannel repeatingChannel =
      NotificationChannel("4", "Repeating Channel", "description4");

  // STREAM DATA
  BehaviorSubject<ReceivedNotification> didReceiveLocalNotificationSubject;
  BehaviorSubject<String> selectNotificationSubject;

  NotificationAppLaunchDetails notificationAppLaunchDetails;

  // SETTINGS
  AndroidInitializationSettings initializationSettingsAndroid;
  IOSInitializationSettings initializationSettingsIOS;
  MacOSInitializationSettings initializationSettingsMacOS;

  final MethodChannel platform = MethodChannel('puntodamar.com/my_boilerplate');

  Future<void> initialize() async {
    didReceiveLocalNotificationSubject =
        BehaviorSubject<ReceivedNotification>();

    selectNotificationSubject = BehaviorSubject<String>();

    flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

    notificationAppLaunchDetails =
        await flutterLocalNotificationsPlugin.getNotificationAppLaunchDetails();

    initializationSettingsAndroid =
        AndroidInitializationSettings('ic_launcher');
    initializationSettingsIOS = IOSInitializationSettings(
        requestAlertPermission: false,
        requestBadgePermission: false,
        requestSoundPermission: false,
        onDidReceiveLocalNotification:
            (int id, String title, String body, String payload) async {
          didReceiveLocalNotificationSubject.add(ReceivedNotification(
              id: id, title: title, body: body, payload: payload));
        });

    initializationSettingsMacOS = MacOSInitializationSettings(
        requestAlertPermission: false,
        requestBadgePermission: false,
        requestSoundPermission: false);

    final InitializationSettings initializationSettings =
        InitializationSettings(
            android: initializationSettingsAndroid,
            iOS: initializationSettingsIOS,
            macOS: initializationSettingsMacOS);

    await flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: (String payload) async {
      if (payload != null) {
        debugPrint('notification payload: $payload');
      }
      selectNotificationSubject.add(payload);
    });

    await configureLocalTimeZone();
  }

  Future<void> configureLocalTimeZone() async {
    tz.initializeTimeZones();
    final String timeZoneName = await platform.invokeMethod('getTimeZoneName');
    tz.setLocalLocation(tz.getLocation(timeZoneName));
  }

  bool get didNotificationLaunchApp =>
      notificationAppLaunchDetails?.didNotificationLaunchApp ?? false;

  void requestPermissions() {
    flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            IOSFlutterLocalNotificationsPlugin>()
        ?.requestPermissions(
          alert: true,
          badge: true,
          sound: true,
        );

    flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            MacOSFlutterLocalNotificationsPlugin>()
        ?.requestPermissions(
          alert: true,
          badge: true,
          sound: true,
        );
  }

  // ON TAP IOS
  void configureDidReceiveLocalNotificationSubject() {
    didReceiveLocalNotificationSubject.stream
        .listen((ReceivedNotification receivedNotification) async {
      Get.to(NotificationPayloadScreen(receivedNotification.payload));
    });
  }

  // ON TAP ANDROID
  void configureSelectNotificationSubject() {
    selectNotificationSubject.stream.listen((String payload) async {
      Get.to(NotificationPayloadScreen(payload));
    });
  }

  Future<void> showNotification({
    @required NotificationChannel notificationChannel,
    @required String title,
    @required dynamic body,
    String payload,
    String ticker,
    String sound,
    Int64List vibrationPattern,
    Color color,
    tz.TZDateTime scheduleOnceAt,
    RepeatInterval repeatInterval,
    Color ledColor,
    int timestamp,
    List<IOSNotificationAttachment> attachments,
    bool showTimestamp = true,
    bool playSound = true,
    bool largeIcon = false,
    bool enableLights = false,
    int ledOnMs = 1500,
    int ledOffMs = 500,
    Importance importance = Importance.max,
    Priority priority = Priority.high,
    String icon = "ic_launcher",
    AndroidNotificationChannelAction channelAction =
        AndroidNotificationChannelAction.createIfNotExists,
  }) async {
    // assert(
    //     (scheduleOnceAt != null && repeatInterval != null) ||
    //         (scheduleOnceAt == null && repeatInterval != null) || (scheduleOnceAt == null && repeatInterval == null),
    //     "Can't repeat a scheduled notification");

    color = color ?? ColorHelper.materialColor.shade500;
    ledColor = ledColor ?? color;

    final List<PendingNotificationRequest> pendingNotificationRequests =
        await flutterLocalNotificationsPlugin.pendingNotificationRequests();

    AndroidNotificationDetails androidPlatformChannelSpecifics =
        AndroidNotificationDetails(notificationChannel.id,
            notificationChannel.name, notificationChannel.description,
            icon: icon,
            enableLights: enableLights,
            ledColor: (enableLights) ? ledColor : null,
            ledOffMs: (enableLights) ? ledOffMs : null,
            ledOnMs: (enableLights) ? ledOnMs : null,
            largeIcon: (largeIcon) ? DrawableResourceAndroidBitmap(icon) : null,
            importance: importance,
            priority: priority,
            ticker: ticker,
            color: color,
            playSound: playSound,
            vibrationPattern: vibrationPattern,
            channelAction: channelAction,
            visibility: NotificationVisibility.public,
            showWhen: showTimestamp,
            when: timestamp,
            sound: (sound != null)
                ? RawResourceAndroidNotificationSound(sound)
                : null);

    IOSNotificationDetails iOSPlatformChannelSpecifics = IOSNotificationDetails(
        badgeNumber: pendingNotificationRequests.length + 1,
        presentSound: playSound,
        attachments: attachments,
        subtitle: body,
        sound: (sound != null) ? '$sound.caf' : null);

    NotificationDetails platformChannelSpecifics = NotificationDetails(
        android: androidPlatformChannelSpecifics,
        iOS: iOSPlatformChannelSpecifics);

    if (scheduleOnceAt != null) {
      await flutterLocalNotificationsPlugin.zonedSchedule(
          pendingNotificationRequests.length,
          title,
          body,
          scheduleOnceAt,
          platformChannelSpecifics,
          uiLocalNotificationDateInterpretation:
              UILocalNotificationDateInterpretation.absoluteTime,
          androidAllowWhileIdle: true);
    } else if (repeatInterval != null) {
      await flutterLocalNotificationsPlugin.periodicallyShow(
          pendingNotificationRequests.length,
          title,
          body,
          repeatInterval,
          platformChannelSpecifics,
          androidAllowWhileIdle: true);
    } else {
      await flutterLocalNotificationsPlugin.show(
        pendingNotificationRequests.length,
        title,
        body,
        platformChannelSpecifics,
      );
    }
  }

  Future<void> cancelNotification(int id) async {
    await flutterLocalNotificationsPlugin.cancel(id);
  }

  Future<void> cancelAllNotification() async {
    await flutterLocalNotificationsPlugin.cancelAll();
  }

  Future<void> deleteChannel(NotificationChannel notificationChannel) async {
    await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            AndroidFlutterLocalNotificationsPlugin>()
        ?.deleteNotificationChannel(notificationChannel.id);
  }
}
