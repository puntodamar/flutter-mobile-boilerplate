import 'package:boilerplate/controllers/storage_controller.dart';
import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:path_provider/path_provider.dart';
import 'package:uuid/uuid.dart';
import 'dart:io';

class FileDownloadResponse {
  final Response response;
  final File file;
  
  FileDownloadResponse(this.response, this.file);
}

class HttpHelper {

  static Future<FileDownloadResponse> download(int queue, String uri, {saveToTemp = false, String saveLocation}) async{
    Dio dio = Dio();
    StorageController storageController = Get.find();
    String filePath;
    File file;
    final String uuid = Uuid().v1();

    if(saveToTemp == true) {
      var tempDir = await getTemporaryDirectory();
      filePath = '${tempDir.path}/tmp$uuid.mp3';
      file = File(filePath);
      if (file.existsSync()) {
        await file.delete();
      }
      await file.create();
    }

    else {
      var dir  = await getExternalStorageDirectory();
      filePath = '${dir.path}/Audio/$uuid.mp3';
    }

    Response response = await dio.download(
      uri,
      filePath,
      onReceiveProgress: (rcv, total) {
        print(
            'received: ${rcv.toStringAsFixed(0)} out of total: ${total.toStringAsFixed(0)}');
        storageController.progress[queue] = ((rcv / total) * 100).floorToDouble();

      },
      deleteOnError: true,
    );

    return FileDownloadResponse(response, file);
  }
}