import 'package:boilerplate/helpers/color_helper.dart';
import 'package:boilerplate/helpers/navigation_helper.dart';
import 'package:boilerplate/helpers/typography_helper.dart';
import 'package:boilerplate/screens/screen.dart';
import 'package:boilerplate/widgets/bordered_image.dart';
import 'package:boilerplate/widgets/menu_container.dart';
import 'package:boilerplate/widgets/navigation_drawer/nav_drawer_item.dart';
import 'package:flutter/material.dart';
import 'package:boilerplate/helpers/extensions.dart';
import 'package:get/get.dart';

class HeroScreen extends StatelessWidget {

  static final String routeName = "/screens/hero";
  final GlobalKey _scaffoldKey = GlobalKey();

  @override
  Widget build(BuildContext context) {

    NavigationHelper.navigationKey = _scaffoldKey;
    return Screen.single(
      screenKey: _scaffoldKey,
      adaptiveNavDrawer: NavDrawerItem(true),
      title: "Hero Example",
      body: MenuContainer(
        mainAxisAlignment: MainAxisAlignment.center,
        child: Column(
          children: [
            Text("Select one : ", style: TypographyHelper.headline6(),).uiPaddingOnly(b: 50),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                GestureDetector(
                  child: Column(
                    children: [
                      Hero(
                        transitionOnUserGestures: true,
                        tag: "hero1",
                        child: BorderedImage.circle(url: "https://picsum.photos/200/300",),
                      ).uiPaddingOnly(b: 20),
                      Text('Image 1', style: TypographyHelper.body2(),)
                    ],
                  ),

                  onTap: () {
                    Get.toNamed("${HeroDestinationScreen.routeName}?title=Image 1&tag=hero1");
                  },
                ),
                GestureDetector(
                  child: Column(
                    children: [
                      Container(
                        color: ColorHelper.materialColor.shade900,
                        width: 100,
                        height: 100,
                      ).uiPaddingOnly(b: 20),
                      Text('A Container', style: TypographyHelper.body2(),)
                    ],
                  ),
                  onTap: () => print("container tapped"),
                ),
                GestureDetector(
                  child: Column(
                    children: [
                      Hero(
                        transitionOnUserGestures: true,
                        tag: "hero2",
                        child: BorderedImage.circle(url: "https://picsum.photos/200/300",),
                      ).uiPaddingOnly(b: 20),
                      Text('Image 2', style: TypographyHelper.body2(),)
                    ],
                  ),
                  onTap: () {
                    Get.toNamed("${HeroDestinationScreen.routeName}?title=Image 2&tag=hero2");
                  },
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}

class HeroDestinationScreen extends StatelessWidget {
  static final String routeName = "/screens/hero-destination";
  final GlobalKey _scaffoldKey = GlobalKey();

  @override
  Widget build(BuildContext context) {

    NavigationHelper.navigationKey = _scaffoldKey;

    final tag = Get.parameters['tag'];
    final String title = Get.parameters['title'];
    return Screen.single(
      screenKey: _scaffoldKey,
      title: title,
      body: Column(
        children: [
          Hero(
            transitionOnUserGestures: true,
            tag: tag,
            child: Placeholder(color: ColorHelper.materialColor.shade500,),
          ),
          Text('Knight Rider, a shadowy flight into the dangerous world of a man who does not exist. Michael Knight, a young loner on a crusade to champion the cause of the innocent, the helpless in a world of criminals who operate above the law. ', style: TypographyHelper.body1(),).uiPaddingAll(20)

        ],
      ),
    );
  }
}

