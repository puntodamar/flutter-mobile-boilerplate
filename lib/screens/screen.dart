import 'package:boilerplate/controllers/nav_drawer_controller.dart';
import 'package:boilerplate/helpers/typography_helper.dart';
import 'package:boilerplate/widgets/navigation_drawer/adaptive_drawer.dart';
import 'package:colorful_safe_area/colorful_safe_area.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:recase/recase.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:boilerplate/helpers/extensions.dart';
import 'package:boilerplate/helpers/color_helper.dart';
import 'package:get/get.dart';

String scaffoldTitle;
Widget scaffoldLeading;
List<Widget> scaffoldActions;
Widget scaffoldBody;
List<Widget> scaffoldBottomNavBarScreens;
List<Widget> scaffoldTopNavBarScreens;
List<BottomNavigationBarItem> scaffoldBottomNavBarItems;
List<Tab> scaffoldTopNavBarItems;
AssetImage scaffoldBackground;
bool scaffoldForceMaterial;
bool scaffoldForceCupertino;
GlobalKey<AdaptiveDrawerState> scaffoldNavDrawerState;
Widget scaffoldAdaptiveNavDrawer;
Widget scaffoldAndroidNavDrawer;
FloatingActionButton scaffoldFloatingActionButton;
FloatingActionButtonLocation scaffoldFloatingActionButtonLocation;
bool scaffoldSliverAppBar;
GlobalKey scaffoldKey;
Widget scaffoldSliverBackground;
List<Widget> scaffoldStackTop;
List<Widget> scaffoldStackBottom;
PlatformScreenType scaffoldPlatformScreenType;

enum PlatformScreenType { Android, iOS, Adaptive }

class Screen extends StatelessWidget {
  final String title;
  final Widget leading;
  final List<Widget> actions;
  final Widget body;
  final GlobalKey screenKey;
  final bool topSafeArea;
  final bool bottomSafeArea;

  List<Widget> topNavBarScreens;
  List<Tab> topNavBarItems;
  List<BottomNavigationBarItem> bottomNavBarItems;
  List<Widget> bottomNavBarScreens;
  final AssetImage background;

  FloatingActionButton floatingActionButton;
  FloatingActionButtonLocation floatingActionButtonLocation;
  Widget adaptiveNavDrawer;
  Widget androidNavDrawer;
  bool forceMaterial;
  bool forceCupertino;
  bool sliverAppBar;
  Widget sliverBackground;
  List<Widget> stackTop;
  List<Widget> stackBottom;
  PlatformScreenType platformScreenType;

  Screen({
    @required this.platformScreenType,
    @required this.screenKey,
    this.title,
    this.leading,
    this.actions,
    this.body,
    this.background,
    this.bottomNavBarScreens,
    this.bottomNavBarItems,
    this.adaptiveNavDrawer,
    this.androidNavDrawer,
    this.sliverBackground,
    this.stackBottom,
    this.stackTop,
    this.sliverAppBar = false,
    this.topSafeArea = true,
    this.bottomSafeArea = true,
  }){
    this.forceMaterial = false;
    this.forceCupertino = false;
  }

  Screen.single({
    @required this.platformScreenType,
    @required this.screenKey,
    this.title,
    this.leading,
    this.actions,
    this.body,
    this.background,
    this.adaptiveNavDrawer,
    this.androidNavDrawer,
    this.sliverBackground,
    this.stackBottom,
    this.stackTop,
    this.floatingActionButton,
    this.sliverAppBar = false,
    this.topSafeArea = true,
    this.bottomSafeArea = true,
  }){
    this.forceMaterial = false;
    this.forceCupertino = false;
  }

  Screen.withMaterialTopNav({
    @required this.topNavBarScreens,
    @required this.topNavBarItems,
    @required this.screenKey,
    this.title,
    this.leading,
    this.actions,
    this.body,
    this.background,
    this.adaptiveNavDrawer,
    this.androidNavDrawer,
    this.floatingActionButton,
    this.floatingActionButtonLocation,
    this.topSafeArea = true,
    this.bottomSafeArea = true,
  }){
    this.sliverAppBar = false;
    this.forceMaterial = true;
    this.forceCupertino = false;
    this.platformScreenType = PlatformScreenType.Android;

  }

  Screen.withCupertinoNavBar(
      {@required this.screenKey,
      this.title,
      this.actions,
      this.body,
      this.background,
      this.leading,
      this.topSafeArea = true,
      this.bottomSafeArea = true,
      }) {
    this.forceMaterial = false;
    this.forceCupertino = true;
    this.platformScreenType = PlatformScreenType.iOS;
  }

  Screen.withAndroidDrawer({
    @required this.screenKey,
    this.androidNavDrawer,
    this.title,
    this.leading,
    this.actions,
    this.body,
    this.background,
    this.floatingActionButton,
    this.floatingActionButtonLocation,
    this.sliverBackground,
    this.sliverAppBar = false,
    this.topSafeArea = true,
    this.bottomSafeArea = true,
  }){
    this.platformScreenType = PlatformScreenType.Android;
  }

  @override
  Widget build(BuildContext context) {
    scaffoldNavDrawerState = GlobalKey();
    Get.put(NavDrawerController()).setNavigationKey(scaffoldNavDrawerState);

    if (bottomNavBarScreens == null && bottomNavBarItems != null ||
        bottomNavBarItems == null && bottomNavBarItems != null)
      throw "BottomNavBarScreen requires BottomNavBarItems";

    if (topNavBarItems == null && topNavBarScreens != null ||
        topNavBarItems != null && topNavBarScreens == null)
      throw "TopNavBarScreen requires TopNavbarItems";

    if (adaptiveNavDrawer != null && leading != null)
      throw "Leading widget cant be used with navdrawer";

    scaffoldBody = body;
    scaffoldTitle = title?.toString()?.titleCase ?? "Title";
    scaffoldActions = actions;
    scaffoldLeading = leading;
    scaffoldBottomNavBarItems = bottomNavBarItems;
    scaffoldBottomNavBarScreens = bottomNavBarScreens;
    scaffoldTopNavBarScreens = topNavBarScreens;
    scaffoldTopNavBarItems = topNavBarItems;
    scaffoldBottomNavBarItems = bottomNavBarItems;
    scaffoldBackground = background;
    scaffoldForceMaterial = forceMaterial;
    scaffoldAdaptiveNavDrawer = adaptiveNavDrawer;
    scaffoldAndroidNavDrawer = androidNavDrawer;
    scaffoldFloatingActionButton = floatingActionButton;
    scaffoldFloatingActionButtonLocation = floatingActionButtonLocation;
    scaffoldSliverAppBar = sliverAppBar;
    scaffoldSliverBackground = sliverBackground;
    scaffoldKey = screenKey;
    scaffoldStackBottom = stackBottom;
    scaffoldStackTop = stackTop;
    scaffoldPlatformScreenType = platformScreenType;




    if (bottomNavBarScreens == null && topNavBarScreens == null)

      return ColorfulSafeArea(
        top: topSafeArea,
        bottom: bottomSafeArea,
        color: ColorHelper.materialColor.shade900,
        child: SingleScreenBasic(),
      );


    else if (bottomNavBarScreens != null){
      NavDrawerController navDrawerController = Get.find();

      navDrawerController.tabScreens = bottomNavBarScreens;
      navDrawerController.bottomNavigationBarItems = bottomNavBarItems;

      
      return ColorfulSafeArea(
        top: topSafeArea,
        bottom: bottomSafeArea,
        color: ColorHelper.materialColor.shade900,
        child: BottomTabbedScreen(),
      );
    }

    else if (forceMaterial && topNavBarScreens != null && topNavBarItems != null){
      NavDrawerController navDrawerController = Get.find();

      navDrawerController.tabScreens = topNavBarScreens;
      navDrawerController.tabs = topNavBarItems;
      return ColorfulSafeArea(
        top: topSafeArea,
        bottom: bottomSafeArea,
        color: ColorHelper.materialColor.shade900,
        child: AndroidTabScreen(),
      );
    }

    else
      throw "Invalid screen";
  }
}

// ignore: must_be_immutable
class ScreenPlatformBody extends StatelessWidget{
  PageStorageBucket bucket;
  Widget body;

  ScreenPlatformBody({this.bucket, this.body});

  @override
  Widget build(BuildContext context) {
    var child = body != null ? body : scaffoldBody;

    return Container(
        decoration: BoxDecoration(
          color: Colors.white,
//        image: DecorationImage(
//          image: AssetImage('assets/images/background.png',),
//          fit: BoxFit.cover,
//        ),
        ),
        child: bucket != null
            ? PageStorage(
          child: child,
          bucket: bucket,
        )
            : child);
  }
}

Widget _platformAppBar(GlobalKey<AdaptiveDrawerState> key) {
  Widget leading;
  Widget cupertinoLeading;
  Widget androidLeading;
  bool autoImplyLeading;
  Widget leadingChild = Icon(
    PlatformIcons(Get.context).back,
    color: Colors.white,
  );

  if (scaffoldAdaptiveNavDrawer != null) {
    autoImplyLeading = false;
    leading = GetBuilder<NavDrawerController>(
      builder: (c) => GestureDetector(
        behavior: HitTestBehavior.translucent,
        child: Icons.menu.asIcon(),
        onTap: () {
          key.currentState.toggle();
        },
      ),
    );
  }
  // CUSTOM LEADING
  else if (scaffoldLeading != null) {
    autoImplyLeading = false;
    leading = scaffoldLeading;

  }
  // AUTO IMPLY LEADING
  else {
    autoImplyLeading = true;
    Function handleTap = () {
      if (Navigator.of(Get.context).canPop())
        Navigator.of(Get.context).pop();
    };

    cupertinoLeading = CupertinoButton(
      padding: EdgeInsets.all(0),
      onPressed: handleTap,
      child: leadingChild,
    );

    androidLeading = IconButton(
      icon: leadingChild,
      onPressed: handleTap,
    );

    leading = PlatformWidgetBuilder(
      cupertino: (_, child, __) => cupertinoLeading,
      material: (_, child, __) => androidLeading,
    );
  }

  print(leading);

  if (scaffoldPlatformScreenType == PlatformScreenType.Android) {
    return AppBar(
      title: Text(scaffoldTitle, style: TypographyHelper.appBarTextStyle()),
      backgroundColor: ColorHelper.platformAppBarBackgroundColor,
      leading: leading,
      actions: scaffoldActions,
      automaticallyImplyLeading: autoImplyLeading,
    );
  }

  else if (scaffoldPlatformScreenType == PlatformScreenType.iOS) {
    return CupertinoNavigationBar(
      middle: Text(scaffoldTitle, style: TypographyHelper.appBarTextStyle()),
      backgroundColor: ColorHelper.platformAppBarBackgroundColor,
      automaticallyImplyLeading: autoImplyLeading,
      leading: leading,
      trailing: scaffoldActions[0],
    );
  }

  else if (scaffoldPlatformScreenType == PlatformScreenType.Adaptive){
    
    return PlatformAppBar(
      backgroundColor: ColorHelper.platformAppBarBackgroundColor,
      automaticallyImplyLeading: false,
      leading: leading,
      cupertino: (_, __) =>
          CupertinoNavigationBarData(actionsForegroundColor: Colors.white),
      // leading: Icons.landscape.asIcon(),
      title: Text(scaffoldTitle, style: TypographyHelper.appBarTextStyle()),
      //    leading: leading
      trailingActions: scaffoldActions,
    );
  }
  return Container();
}

class SingleScreenBasic extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget scaffold;
    Widget appBarLeading;
    Widget body;
    scaffoldNavDrawerState = GlobalKey();
    Get.put(NavDrawerController()).setNavigationKey(scaffoldNavDrawerState);

    if (scaffoldAdaptiveNavDrawer != null) {
      appBarLeading = GetBuilder<NavDrawerController>(
        builder: (c) => GestureDetector(
          behavior: HitTestBehavior.translucent,
          child: Icons.menu.asIcon(),
          onTap: () {
            scaffoldNavDrawerState.currentState.toggle();

          },
        ),
      );
    } else
      appBarLeading = scaffoldLeading;

    if (scaffoldSliverAppBar)
      body = ScreenSliverAppBar(body: ScreenPlatformBody(), leading: appBarLeading,);
    else
      body = ScreenPlatformBody();


    if (scaffoldPlatformScreenType == PlatformScreenType.Android) {
      return Scaffold(
        key: scaffoldKey,
        appBar: (scaffoldSliverAppBar) ? null : _platformAppBar(scaffoldNavDrawerState),
        drawer: scaffoldAndroidNavDrawer,
        floatingActionButton:  scaffoldFloatingActionButton,
        floatingActionButtonLocation: scaffoldFloatingActionButtonLocation,
        body: (scaffoldStackTop != null || scaffoldStackBottom != null) ? Stack(
          children: [
            if( scaffoldStackTop != null && scaffoldStackTop.length > 0) ...scaffoldStackTop,
            body,
            if(scaffoldStackBottom != null && scaffoldStackBottom.length > 0) ...scaffoldStackBottom
          ],
        ) : body,
      );
    }

    else if (scaffoldPlatformScreenType == PlatformScreenType.iOS) {
      scaffold = CupertinoPageScaffold(
        key: scaffoldKey,
          child: (scaffoldStackTop != null || scaffoldStackBottom != null) ? Stack(
        children: [
          if( scaffoldStackTop != null && scaffoldStackTop.length > 0) ...scaffoldStackTop,
          body,
          if(scaffoldStackBottom != null && scaffoldStackBottom.length > 0) ...scaffoldStackBottom
        ],
      ) : body
      );

      return (scaffoldAdaptiveNavDrawer != null) ? AdaptiveDrawer(
        key: scaffoldNavDrawerState,
        scaffold: scaffold,
        navDrawer: scaffoldAdaptiveNavDrawer,
      ) : scaffold;
    }

    else if (scaffoldPlatformScreenType == PlatformScreenType.Adaptive) {
      scaffold = PlatformScaffold(
        key: scaffoldKey,
        appBar: (scaffoldSliverAppBar) ? null : _platformAppBar(scaffoldNavDrawerState),
        body: (scaffoldStackTop != null || scaffoldStackBottom != null) ? Stack(
          children: [
            if( scaffoldStackTop != null && scaffoldStackTop.length > 0) ...scaffoldStackTop,
            body,
            if(scaffoldStackBottom != null && scaffoldStackBottom.length > 0) ...scaffoldStackBottom
          ],
        ) : body,
      );


      return (scaffoldAdaptiveNavDrawer != null) ? AdaptiveDrawer(
        key: scaffoldNavDrawerState,
        scaffold: scaffold,
        navDrawer: scaffoldAdaptiveNavDrawer,
      ) : (scaffoldAndroidNavDrawer != null) ? scaffold.forceAsMaterialWidget() : scaffold;
    }

    return Container();
  }
}

class AndroidTabScreen extends StatefulWidget {
  @override
  _AndroidTabScreenState createState() => _AndroidTabScreenState();
}

class _AndroidTabScreenState extends State<AndroidTabScreen> {
  final PageStorageBucket bucket = PageStorageBucket();
  TabController controller;


  // @override
  // void initState() {
  //   controller =
  //   new TabController(vsync: this, length: scaffoldTopNavBarScreens.length);
  //   super.initState();
  // }

  @override
  Widget build(BuildContext context) {
    Widget appBarLeading;
    scaffoldNavDrawerState = GlobalKey();
    NavDrawerController navDrawerController = Get.find();

    navDrawerController.setNavigationKey(scaffoldNavDrawerState);

    if (scaffoldAdaptiveNavDrawer != null) {
      appBarLeading = GetBuilder<NavDrawerController>(
        builder: (c) => GestureDetector(
          behavior: HitTestBehavior.translucent,
          child: Icons.menu.asIcon(),
          onTap: () {
            scaffoldNavDrawerState.currentState.toggle();
          },
        ),
      );
    } else
      appBarLeading = scaffoldLeading;

    MaterialNavBarData materialData = ColorHelper.materialNavBarData();

    TabBarView body = TabBarView(
      controller: controller,
      children: navDrawerController.tabScreens ?? [Container()],
    );

    return DefaultTabController(
      length: navDrawerController.tabScreens.length,
      child: Scaffold(
        key: scaffoldKey,
        drawer: (scaffoldAdaptiveNavDrawer != null)
            ? scaffoldAdaptiveNavDrawer
            : scaffoldAndroidNavDrawer,
        appBar: AppBar(
          automaticallyImplyLeading: true,
          actions: scaffoldActions,
          leading: appBarLeading,
          backgroundColor: materialData.backgroundColor,
          //judul
          title: Text(scaffoldTitle ?? ""),

          bottom: TabBar(
            indicatorColor: ColorHelper.accentColor,
            controller: controller,
            tabs: navDrawerController.tabs ??
                [
                  Tab(
                    icon: new Icon(Icons.email),
                    text: "Example Tab",
                  )
                ],
          ),
        ),
        floatingActionButton: scaffoldFloatingActionButton,
        floatingActionButtonLocation: scaffoldFloatingActionButtonLocation ??
            FloatingActionButtonLocation.endFloat,
        body: ScreenPlatformBody(bucket: bucket, body: body ?? Container(),)
      ).forceAsMaterialWidget(),
    );
  }
}

class BottomTabbedScreen extends StatefulWidget {
  @override
  _BottomTabbedScreenState createState() => _BottomTabbedScreenState();
}

class _BottomTabbedScreenState extends State<BottomTabbedScreen>
    with SingleTickerProviderStateMixin {
  final PageStorageBucket bucket = PageStorageBucket();
  PlatformTabController controller;

  @override
  void initState() {
    controller = new PlatformTabController(initialIndex: 0);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    scaffoldNavDrawerState = GlobalKey();
    NavDrawerController navDrawerController = Get.find();
    navDrawerController.setNavigationKey(scaffoldNavDrawerState);

    PlatformTabScaffold scaffold = PlatformTabScaffold(
      key: scaffoldKey,
      iosContentBottomPadding: true,
      iosContentPadding: true,
      tabController: controller,
      appBarBuilder: (_, index) => _platformAppBar(scaffoldNavDrawerState),
      materialTabs: (_, __) => ColorHelper.materialNavBarData(),
      bodyBuilder: (_, index) {
        Widget body;
        try {
          body = navDrawerController.tabScreens[index];
        } catch (e) {
          body = Container();
        }
        return ScreenPlatformBody(bucket: bucket, body: body,);
      },
      items: navDrawerController.bottomNavigationBarItems,
    );

    if (scaffoldAdaptiveNavDrawer != null) {
      return AdaptiveDrawer(
        key: scaffoldNavDrawerState,
        navDrawer: scaffoldAdaptiveNavDrawer,
        scaffold: scaffold,
      );
    } else
      return scaffold;
  }
}

class ScreenSliverAppBar extends StatelessWidget{
  final Widget body;
  final Widget leading;
  ScreenSliverAppBar({this.body, this.leading});

  @override
  Widget build(BuildContext context) {

    NestedScrollView android = NestedScrollView(
      headerSliverBuilder:
          (BuildContext context, bool innerBoxIsScrolled) {
        return <Widget>[
          SliverAppBar(
            backgroundColor: ColorHelper.materialColor.shade500,
            automaticallyImplyLeading: true,
            leading: leading,
            actions: scaffoldActions,
            expandedHeight: 0.3.sh,
            floating: true,
            pinned: true,
            flexibleSpace: FlexibleSpaceBar(
                centerTitle: true,
                title: Text(
                  scaffoldTitle,
                ),
                background: scaffoldSliverBackground == null ? Image.network(
                  "https://picsum.photos/600/500",
                  fit: BoxFit.fill,
                ) : scaffoldSliverBackground
            ),
          )
        ];
      },
      body: body,
    );

    NestedScrollView ios = NestedScrollView(
      headerSliverBuilder:
          (BuildContext context, bool innerBoxIsScrolled) {
        return <Widget>[
          CupertinoSliverNavigationBar(
           // heroTag: 'hero1',
            largeTitle: Text(
              scaffoldTitle,
              style: TypographyHelper.sliverAppBarTextStyle(),
            ).align(),
            backgroundColor: ColorHelper.materialColor.shade500,
            automaticallyImplyLeading: true,
            leading: leading,
            trailing: scaffoldActions[0],
          ),
        ];
      },
      body: body,
    );

    if (scaffoldPlatformScreenType == PlatformScreenType.Android) {
      return android;
    }

    else if (scaffoldPlatformScreenType == PlatformScreenType.iOS) {
      return ios;
    }
    else return PlatformWidgetBuilder(
          cupertino: (_, child, __) => ios,
          material: (_, child, __) => android
      );
  }

}

