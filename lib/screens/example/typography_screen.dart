import 'package:boilerplate/helpers/color_helper.dart';
import 'package:boilerplate/helpers/typography_helper.dart';
import 'package:boilerplate/screens/screen.dart' show Screen;
import 'package:boilerplate/widgets/buttons/adaptive_button.dart';
import 'package:boilerplate/widgets/menu_container.dart';
import 'package:boilerplate/widgets/navigation_drawer/nav_drawer_item.dart';
import 'package:flutter/material.dart';
import 'package:boilerplate/helpers/extensions.dart';

class TypoGraphyScreen extends StatelessWidget {

  static String routeName = '/screens/typography-screen';
  final GlobalKey _scaffoldKey = GlobalKey();


  @override
  Widget build(BuildContext context) {
    return Screen.single(
      screenKey: _scaffoldKey,
      adaptiveNavDrawer: NavDrawerItem(true),
      title: "Typography",
      body: MenuContainer(
        child: Column(
          children: [
            Text("Text H1", style: TypographyHelper.headline1(),),
            Text("Text H2", style: TypographyHelper.headline2(),),
            Text("Text H3", style: TypographyHelper.headline3(),),
            Text("Text H4", style: TypographyHelper.headline4(),),
            Text("Text H5", style: TypographyHelper.headline5(),),
            Text("Text H6", style: TypographyHelper.headline6(),),

            Text("Subtitle 1", style: TypographyHelper.subtitle1(),),
            Text("Subtitle 2", style: TypographyHelper.subtitle2(),),

            Text("Body 1", style: TypographyHelper.body1(),),
            Text("Body 2", style: TypographyHelper.body2(),),

            Text("Caption", style: TypographyHelper.caption(),),


            AdaptiveButton(text: "Button Text",)

          ],
        ),
      ),
    );
  }
}
