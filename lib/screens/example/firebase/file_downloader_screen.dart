import 'dart:io';

import 'package:boilerplate/screens/screen.dart' show Screen, PlatformScreenType;
import 'package:boilerplate/widgets/menu_container.dart';
import 'package:boilerplate/widgets/navigation_drawer/nav_drawer_item.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:image_downloader/image_downloader.dart';
import 'package:path_provider/path_provider.dart';

class FileDownloaderScreen extends StatefulWidget {

  static final String routeName = '/screens/firebase/file-downloader';
  final GlobalKey _scaffoldKey = GlobalKey();

  @override
  _FileDownloaderScreenState createState() => _FileDownloaderScreenState();
}

class _FileDownloaderScreenState extends State<FileDownloaderScreen> {

  @override
  Widget build(BuildContext context) {
    return Screen.single(
      title: "File Downloader",
      screenKey: widget._scaffoldKey,
      platformScreenType: PlatformScreenType.Android,
      adaptiveNavDrawer: NavDrawerItem(true),
        floatingActionButton: FloatingActionButton(
            child: Icon(Icons.play_arrow),
            onPressed: () async {
              // downloadFile(uri, filename);
              await ImageDownloader.downloadImage("https://raw.githubusercontent.com/wiki/ko2ic/image_downloader/images/flutter.png",
                destination: AndroidDestinationType.custom(directory: 'sample')
                  ..inExternalFilesDir()
                  ..subDirectory("custom/sample.jpg"),
              );
              // await ImageDownloader.downloadImage("https://raw.githubusercontent.com/wiki/ko2ic/image_downloader/images/flutter.png");
            }),
      body: MenuContainer(
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
              ],
            ),
          ),
        ),
      ),
    );
  }
}
