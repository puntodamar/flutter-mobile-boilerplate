import 'package:boilerplate/controllers/firebase_controller.dart';
import 'package:boilerplate/controllers/form_controller.dart';
import 'package:boilerplate/controllers/image_picker_controller.dart';
import 'package:boilerplate/helpers/color_helper.dart';
import 'package:boilerplate/helpers/dialog_helper.dart';
import 'package:boilerplate/helpers/validation_helper.dart';
import 'package:boilerplate/widgets/buttons/adaptive_button.dart';
import 'package:boilerplate/widgets/forms/adaptive_text_field.dart';
import 'package:boilerplate/widgets/forms/form_box.dart';
import 'package:boilerplate/widgets/image_selector.dart';
import 'package:boilerplate/widgets/navigation_drawer/nav_drawer_item.dart';
import 'package:flutter/material.dart';
import 'package:boilerplate/screens/screen.dart' show Screen;
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:boilerplate/helpers/extensions.dart';

class FirebaseProfileScreen extends StatefulWidget {
  static String routeName = '/screens/firebase/profile';
  final GlobalKey _scaffoldKey = GlobalKey();

  @override
  _FirebaseProfileScreenState createState() => _FirebaseProfileScreenState();
}

class _FirebaseProfileScreenState extends State<FirebaseProfileScreen> {
  final _formKey = GlobalKey<FormState>();
  FormController formController = Get.find();
  FirebaseController firebaseController = Get.find();
  ImagePickerController imagePickerController = Get.find();

  TextEditingController emailController = TextEditingController();
  TextEditingController displayNameController = TextEditingController();
  TextEditingController phoneNumberController = TextEditingController();

  final FocusNode displayNameFocusNode = new FocusNode();
  List<TextEditingController> controllers = [];

  Map<String, Object> _formData = {
    'email': '',
    'photoUrl': '',
    'displayName': '',
    'phone': ''
  };

  @override
  void initState() {
    emailController.text = firebaseController.currentUser.email;
    displayNameController.text =
        firebaseController.currentUser.displayName != null
            ? firebaseController.displayName
            : "-";

    phoneNumberController.text =
        firebaseController.currentUser.phoneNumber != null
            ? firebaseController.currentUser.phoneNumber
            : "-";

    controllers = [
      displayNameController,
      emailController,
      phoneNumberController
    ];

    KeyboardVisibility.onChange.listen((bool visible) {
      formController.setKeyboard(visible);
      // if(!formController.isEditing) formController.setIsTyping(true);
    });
    super.initState();
  }

  void _fillData(String key, Object value) {
    print("fill data");
    _formData[key] = value;
  }

  @override
  Widget build(BuildContext context) {


    return Screen.single(
      screenKey: widget._scaffoldKey,
      adaptiveNavDrawer: NavDrawerItem(true),
      title: "Firebase Profile",
      body: SingleChildScrollView(
        // reverse: true,
        child: FormBox(
          color: Colors.white,
          formKey: _formKey,
          inputFields: [
            Container(
              decoration: BoxDecoration(
                border: Border.all(
                    color: ColorHelper.materialColor.shade500, width: 10.w),
              ),
              child: GetBuilder<ImagePickerController>(
                builder: (c) {
                  if (c.imageAsBytes != null) {
                    Image image = (c.imageAsBytes != null)
                        ? Image.memory(
                            c.imageAsBytes,
                            width: 250.w,
                            fit: BoxFit.fill,
                          )
                        : firebaseController.currentUser.photoURL != null
                            ? Image.network(
                                firebaseController.currentUser.photoURL,
                                width: 250.w,
                                fit: BoxFit.fill,
                              )
                            : Placeholder(color: ColorHelper.materialColor.shade500,).boxed(width: 250.w, height: 250.w);
                    c.imageAsBytes = null;
                    // _fillData('photo', c.imageAsBytes);
                    return image;
                  }
                  return firebaseController.currentUser.photoURL != null
                      ? Image.network(
                    firebaseController.currentUser.photoURL,
                    width: 250.w,
                    fit: BoxFit.fill,
                  )
                      : Placeholder(color: ColorHelper.materialColor.shade500,).boxed(width: 250.w, height: 250.w,);
                },
              ),
            ),
            GetBuilder<FormController>(
              builder: (c) {
                if (c.isEditing)
                  return AdaptiveButton(
                    text: "Change",
                    width: 150.w,
                    height: 30,
                    suffixIcon: Icon(
                      Icons.camera_alt,
                      size: 10,
                    ),
                    onPressed: () => DialogHelper.showAlertDialog(
                        title: "Select From",
                        content: ImageSelector(
                          camera: true,
                          gallery: true,
                        )),
                  ).uiPaddingOnly(b: 100);
                else
                  return Container().uiPaddingOnly(b: 100);
              },
            ),
            GetBuilder<FormController>(
              // init: FormController(),
              builder: (c) => AdaptiveTextField.android(
                focusNode: displayNameFocusNode,
                containerColor: Colors.white,
                floatingLabelBehavior: FloatingLabelBehavior.always,
                enabled: c.isEditing,
                controller: controllers[0],
                dataKey: 'displayName',
                labelText: "Display Name",
                validators: [Validation.empty],
                keyboardType: TextInputType.name,
                onSaved: _fillData,
              ).uiPaddingOnly(b: 30),
            ),
            GetBuilder<FormController>(
              // init: FormController(),
              builder: (c) => AdaptiveTextField.android(
                enabled: c.isEditing,
                containerColor: Colors.white,
                floatingLabelBehavior: FloatingLabelBehavior.always,
                controller: controllers[1],
                dataKey: 'email',
                labelText: "Email",
                validators: [Validation.empty, Validation.email],
                keyboardType: TextInputType.emailAddress,
                onSaved: _fillData,
              ).uiPaddingOnly(b: 30),
            ),
            GetBuilder<FormController>(
              // init: FormController(),
              builder: (c) => AdaptiveTextField.android(
                containerColor: Colors.white,
                floatingLabelBehavior: FloatingLabelBehavior.always,
                enabled: c.isEditing,
                controller: controllers[2],
                dataKey: 'phone',
                labelText: "Phone Number",
                validators: [Validation.empty],
                keyboardType: TextInputType.phone,
                onSaved: _fillData,
              ).uiPaddingOnly(b: 30),
            ),
            Container(
              color: ColorHelper.materialColor.shade50,
              height: 1000,
            ),
          ],
        ),
      ),
      stackBottom: [
        GetBuilder<FormController>(
          // init: FormController(),
          builder: (c) => c.isKeyboardOpen
              ? Container()
              : Container(
                  color: Colors.white,
                  child: GetBuilder<FormController>(
                    builder: (c) {
                      return AdaptiveButton(
                        getxController: firebaseController,
                        height: 150.h,
                        width: double.infinity,
                        color: c.isEditing
                            ? Colors.green
                            : ColorHelper.materialColor.shade500,
                        text: c.isEditing ? "Finish" : 'Update',
                        onPressed: () {
                          // edit.update((val) { val = !val;});
                          // print("obx: ${edit.value}");
                          if (c.isEditing) {
                            if (_formKey.currentState.validate()) {
                              controllers
                                  .where((element) => element.text == "")
                                  .forEach((c) => c.text = "-");
                              c.toggleIsEditing();
                              _formKey.currentState.save();
                              firebaseController.updateUser(_formData);
                            }
                          } else {
                            c.toggleIsEditing();
                            controllers
                                .where((element) => element.text == "-")
                                .forEach((c) => c.text = "");
                          }
                        },
                      );
                    },
                  ),
                ).align(Alignment.bottomCenter).uiPaddingAll(30),
        )
      ],
    );
  }
}
