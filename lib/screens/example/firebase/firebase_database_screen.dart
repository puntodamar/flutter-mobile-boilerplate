import 'dart:math';

import 'package:boilerplate/controllers/listeners/database/random_string_list_database_listener.dart';
import 'package:boilerplate/helpers/dialog_helper.dart';
import 'package:boilerplate/helpers/extensions.dart';
import 'package:boilerplate/helpers/typography_helper.dart';
import 'package:boilerplate/models/random_string_model.dart';
import 'package:boilerplate/screens/screen.dart' show Screen;
import 'package:boilerplate/widgets/dismissible_background.dart';
import 'package:boilerplate/widgets/menu_container.dart';
import 'package:boilerplate/widgets/navigation_drawer/nav_drawer_item.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/animation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

String generateRandomString(int len) {
  var r = Random();
  const _chars =
      'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
  return List.generate(len, (index) => _chars[r.nextInt(_chars.length)]).join();
}

// ignore: must_be_immutable
class FirebaseDatabaseScreen extends StatelessWidget {
  static String routeName = '/screens/firebase/database';
  final GlobalKey _scaffoldKey = GlobalKey();
  final GlobalKey<AnimatedListState> listKey = GlobalKey<AnimatedListState>();
  final DatabaseReference database = FirebaseDatabase.instance.reference().child("random-string-list");
  final RandomStringListDatabaseListener randomStringListDatabaseListener =
  Get.put(RandomStringListDatabaseListener());

  @override
  Widget build(BuildContext context) {
    randomStringListDatabaseListener.setListKey(listKey);
    return Screen.withAndroidDrawer(
      screenKey: _scaffoldKey,
      androidNavDrawer: NavDrawerItem(false),
      title: "Firebase Stream Database",
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.black,
        child: Icons.add.asIcon(),
        onPressed: () async {
          await database.push().set({'randomString': generateRandomString(10), 'dateTime': DateTime.now().toIso8601String() });
          // listKey.currentState.insertItem(randomStringListDatabaseListener.list.length, duration: const Duration(milliseconds: 500));
          // randomStringListDatabaseListener.list.add(RandomStringModel(generateRandomString(20), DateTime.now()));
        },
      ),
      body: MenuContainer(
        mainAxisAlignment: MainAxisAlignment.start,
        scrollable: true,
        child: Column(
          children: [
            Text(
              'Current data in database : ',
              style: TypographyHelper.headline4(),
            ),
            Text(
              'Swipe to save / remove',
              style: TypographyHelper.subtitle1(),
            ).uiPaddingOnly(b: 30),

            SingleChildScrollView(
              child: GetBuilder<RandomStringListDatabaseListener>(
                builder: (c) {
                  return Container(
                      height: 0.8.sh,
                      child: AnimatedList(
                        key: listKey,
                        initialItemCount:
                        c.list.length,
                        itemBuilder: (context, item, animation) {
                          return slideIt(context, item, animation);
                        },
                      )
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget slideIt(BuildContext context, int index, animation) {
    RandomStringModel model = randomStringListDatabaseListener.list[index];
    String text = model.randomString;
    return SlideTransition(
      position: Tween<Offset>(
        begin: const Offset(-1, 0),
        end: Offset(0, 0),
      ).animate(animation),
      child: SizedBox(
        // Actual widget to display
        height: 150.h,
        child: Dismissible(
          background: DismissibleBackground(
            text: "Save",
            position: DismissableBackgroundPosition.Left,
            icon: Icons.save.asIcon(),
            color: Colors.green,
          ),
          secondaryBackground: DismissibleBackground(
            text: "Delete",
            position: DismissableBackgroundPosition.Right,
            icon: Icons.remove_circle_outline.asIcon(),
            color: Colors.black,
          ),
          key: Key("$text $index"),
          child: Card(
            color: Colors.primaries[index % Colors.primaries.length],
            child: InkWell(
              child: Center(
                child: Text(text),
              ),
              onTap: () {},
            ),
          ),
          confirmDismiss: (direction) async {
            // if (list.length == 0) return;
            if (direction == DismissDirection.endToStart) {
              bool res = await DialogHelper.showAlertDialog(
                  title: "WARNING!",
                  content: Text('Are you sure want to delete $text ?'),
                  actions: [
                    PlatformDialogAction(
                      child: Text(
                        "Cancel",
                        style: TextStyle(color: Colors.black),
                      ),
                      onPressed: () {
                        Navigator.of(context, rootNavigator: true).pop(false);
                      },
                    ),
                    PlatformDialogAction(
                      child: Text(
                        "Delete",
                        style: TextStyle(color: Colors.red),
                      ),
                      onPressed: () {
                        Navigator.of(context, rootNavigator: true).pop(true);
                        listKey.currentState.removeItem(
                            index, (_, animation) => Container(),
                            duration: const Duration(milliseconds: 500));
                        randomStringListDatabaseListener.list.removeAt(index);
                      },
                    ),
                  ]);
              return false;
            } else {
              listKey.currentState.removeItem(
                  index, (_, animation) => Container(),
                  duration: const Duration(milliseconds: 500));
              DialogHelper.showModalSheet(Text(
                'Saved to database',
                style: TypographyHelper.body1(),
              ));
              randomStringListDatabaseListener.list.removeAt(index);
              return true;
            }
          },
        ),
      ),
    );
  }

}



