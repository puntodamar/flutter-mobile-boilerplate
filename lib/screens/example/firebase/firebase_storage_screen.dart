import 'package:boilerplate/controllers/storage_controller.dart';
import 'package:boilerplate/screens/screen.dart' show Screen, PlatformScreenType;
import 'package:boilerplate/widgets/menu_container.dart';
import 'package:boilerplate/widgets/navigation_drawer/nav_drawer_item.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class FirebaseStorageScreen extends StatelessWidget {

  static final String routeName = '/screens/firebase/storage-screen';
  final GlobalKey _scaffoldKey = GlobalKey();
  final StorageController fileDownloaderController = Get.find();

  StorageReference storage;

  @override
  Widget build(BuildContext context) {


    // fileDownloaderController.downloadFromUrl("https://image.freepik.com/free-photo/close-up-blank-old-concrete-wall_23-2147856094.jpg", openFileFromNotification: true);
    // print(fileDownloaderController.loadTasks());
    return Screen.single(
      platformScreenType: PlatformScreenType.Adaptive,
      screenKey: _scaffoldKey,
      title: "Firebase Storage",
      adaptiveNavDrawer: NavDrawerItem(true),
      body: MenuContainer(
        child: Container(
          child: Text('Still on progress waiting for firebase fle listing API'),
        ),
      ),
    );
  }
}
