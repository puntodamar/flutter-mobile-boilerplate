import 'package:boilerplate/helpers/navigation_helper.dart';
import 'package:boilerplate/helpers/shape_helper.dart';
import 'package:boilerplate/helpers/typography_helper.dart';
import 'package:boilerplate/helpers/validation_helper.dart';
import 'package:boilerplate/screens/screen.dart' show Screen;
import 'package:boilerplate/widgets/forms/adaptive_text_field.dart';
import 'package:boilerplate/widgets/buttons/adaptive_button.dart';
import 'package:boilerplate/widgets/forms/form_box.dart';
import 'package:boilerplate/widgets/menu_container.dart';
import 'package:boilerplate/widgets/navigation_drawer/nav_drawer_item.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:boilerplate/helpers/extensions.dart';

// ignore: must_be_immutable
class FormFieldScreen extends StatelessWidget {
  static final String routeName = '/screens/form-field';
  final _formKey = GlobalKey<FormState>();
  final GlobalKey _scaffoldKey = GlobalKey();
  Map<String, Object> _formData = {
    'username': '',
    'password': '',
    'email': ''
  };

  @override
  Widget build(BuildContext context) {
    final FocusNode passwordFocusNode = new FocusNode();
    final FocusNode emailFocusNode = new FocusNode();
    NavigationHelper.navigationKey = _scaffoldKey;

    void _fillData(String key, Object value) {
      _formData[key] = value;
    }

    return Screen.single(
        screenKey: _scaffoldKey,
        adaptiveNavDrawer: NavDrawerItem(true),
        title: "Form Field Screen",
        body: MenuContainer(
          scrollable: true,
          child: FormBox(
            formKey: _formKey,
            showLogo: true,
            showBackButton: true,
            subtitles: [Text('enter your username and password', style: TypographyHelper.subtitle1().copyWith(fontStyle: FontStyle.italic))],
            inputFields: [
              AdaptiveTextField(
                dataKey: 'username',
                autoFocus: true,
                nextField: passwordFocusNode,
                prefix: Icons.person.asIcon(false),
                hintText: "Adaptive text field (username)",
                suffix: Icons.person.asIcon(false),
                onSaved: _fillData,
              ).uiPaddingOnly(b: 20),
              AdaptiveTextField.android(
                focusNode: passwordFocusNode,
                nextField: emailFocusNode,
                dataKey: 'password',
                labelText: "Password",
                hintText: "Android",
                helperText: "This is a helper text",
                counterText: "characters",
                obscureText: true,
                validators: [Validation.empty],
//                      errorText: "Error!",
                prefix: Icons.send.asIcon(false),
                suffix: Icons.person.asIcon(false),
                onSaved: _fillData,
              ).uiPaddingOnly(b: 20),
              AdaptiveTextField.android(
                focusNode: emailFocusNode,
                shape: TextInputShape.rounded,
                dataKey: 'email',
                labelText: "Email",
                hintText: "E-mail address",
                keyboardType: TextInputType.emailAddress,
                validators: [Validation.email],
                onSaved: _fillData,
              ).uiPaddingOnly(b: 20),
              AdaptiveButton(
                  text: 'Submit',
                  onPressed: () {
                    if (!_formKey.currentState.validate()) return;
//                    _formKey.currentState.save();
                    _formKey.currentState.reset();
                    print("Form submitted!");
                  })
            ],
//                height: 200,
          ),
        )
    );
  }
}



