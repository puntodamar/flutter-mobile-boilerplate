import 'dart:typed_data';

import 'package:boilerplate/controllers/notification_controller.dart';
import 'package:boilerplate/controllers/storage_controller.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart' show Importance, Priority,RepeatInterval, IOSNotificationAttachment;

import 'package:boilerplate/screens/screen.dart'
    show Screen, PlatformScreenType;
import 'package:boilerplate/widgets/buttons/adaptive_button.dart';
import 'package:boilerplate/widgets/navigation_drawer/nav_drawer_item.dart'
    show NavDrawerItem;
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:timezone/data/latest.dart' as tz;
import 'package:timezone/timezone.dart' as tz;

class NotificationScreen extends StatefulWidget {
  static final String routeName = "/screens/notifications";
  final GlobalKey _scaffoldKey = GlobalKey();

  @override
  _NotificationScreenState createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> {
  NotificationController notificationController = Get.find();
  StorageController storageController = Get.find();

  @override
  void initState(){
    super.initState();
    notificationController.requestPermissions();
    notificationController.configureDidReceiveLocalNotificationSubject();
    notificationController.configureSelectNotificationSubject();
    // notificationController.configureLocalTimeZone();
  }

  @override
  void dispose() {
    notificationController.didReceiveLocalNotificationSubject.close();
    notificationController.selectNotificationSubject.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    final Int64List vibrationPattern = Int64List(4);
    vibrationPattern[0] = 0;
    vibrationPattern[1] = 1000;
    vibrationPattern[2] = 5000;
    vibrationPattern[3] = 2000;

    return Screen.single(
      title: "Notification Screen",
      screenKey: widget._scaffoldKey,
      platformScreenType: PlatformScreenType.Adaptive,
      adaptiveNavDrawer: NavDrawerItem(true),
      body: SingleChildScrollView(
        child: Column(
          children: [
            AdaptiveButton(
              text: "Basic Notification",
              onPressed: () => notificationController.showNotification(
                  notificationChannel: notificationController.basicChannel,
                  title: "Basic Notification Title",
                  body: "Basic Notification Body",
                  payload: "Basic Notification Payload",
                  ticker: "Basic Notification Ticker",),
            ),
            AdaptiveButton(
              text: "Custom Sound",
              onPressed: () => notificationController.showNotification(
                  notificationChannel: notificationController.customSoundChannel,
                  importance: Importance.defaultImportance,
                  priority: Priority.defaultPriority,
                  title: "Custom Sound Title",
                  body: "Custom Sound Body",
                  sound: "got_it_done"),
            ),
            AdaptiveButton(
              text: "Custom Vibration, Icon, LED",
              onPressed: () => notificationController.showNotification(
                  notificationChannel: notificationController.customVibrationChannel,
                  vibrationPattern: vibrationPattern,
                  enableLights: true,
                  largeIcon: true,
                  title: "Custom Vibration, Icon, LED Title",
                  body: "Custom Vibration, Icon, LED Body",),
            ),
            AdaptiveButton(
              text: "Scheduled Notification",
              onPressed: () => notificationController.showNotification(
                notificationChannel: notificationController.basicChannel,
                scheduleOnceAt: tz.TZDateTime.now(tz.local).add(const Duration(seconds: 5)),
                title: "Scheduled Notification Title",
                body: "Scheduled Notification Body",),
            ),
            AdaptiveButton(
              text: "Repeat Notification Every Minute",
              onPressed: () => notificationController.showNotification(
                notificationChannel: notificationController.repeatingChannel,
                repeatInterval: RepeatInterval.everyMinute,
                title: "Repeating Notification Title",
                body: "Repeating Notification Body",),
            ),
            AdaptiveButton(
              text: "Cancel All Notification",
              onPressed: () => notificationController.cancelAllNotification()
            ),
            AdaptiveButton(
                text: "iOS Notification Attachment",
                onPressed: () async {

                  String attachmentPath = await storageController.downloadAndSaveFile(
                      'http://via.placeholder.com/600x200', 'bigPicture.jpg');
                  List<IOSNotificationAttachment> attachments = [
                    IOSNotificationAttachment(attachmentPath)
                  ];
                  notificationController.showNotification(
                    notificationChannel: notificationController.basicChannel,
                    title: "iOS Notification Attachment Title",
                    body: "iOS Notification Attachment Body",);
                }
            )
          ],
        ),
      ),
    );
  }
}

class NotificationPayloadScreen extends StatefulWidget {
  const NotificationPayloadScreen(
    this.payload, {
    Key key,
  }) : super(key: key);

  final String payload;

  @override
  State<StatefulWidget> createState() => NotificationPayloadScreenState();
}

class NotificationPayloadScreenState extends State<NotificationPayloadScreen> {
  String _payload;

  @override
  void initState() {
    super.initState();
    _payload = widget.payload;
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: true,
          title: Text('Notification Payload Screen'),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text('Payload: ${_payload}'),
              RaisedButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: const Text('Go back!'),
              )
            ],
          ),
        ),
      );
}
