import 'package:boilerplate/controllers/firebase_controller.dart';
import 'package:boilerplate/helpers/color_helper.dart';
import 'package:boilerplate/helpers/dialog_helper.dart';
import 'package:boilerplate/helpers/typography_helper.dart';
import 'package:boilerplate/helpers/validation_helper.dart';
import 'package:boilerplate/widgets/adaptive_circular_progress_indicator.dart';
import 'package:boilerplate/widgets/buttons/adaptive_button.dart';
import 'package:boilerplate/widgets/forms/adaptive_text_field.dart';
import 'package:boilerplate/widgets/forms/form_box.dart';
import 'package:boilerplate/widgets/menu_container.dart';
import 'package:boilerplate/widgets/social_login.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:boilerplate/helpers/extensions.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class LoginScreen extends StatefulWidget {
  static final String routeName = "/login";

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _loginFormKey = GlobalKey<FormState>();
  final _registerFormKey = GlobalKey<FormState>();

  Map<String, Object> _formData = {'password': '', 'email': ''};

  FirebaseAuth firebaseAuth = FirebaseAuth.instance;

  FirebaseController firebaseController = Get.find();

  void _fillData(String key, Object value) {
    _formData[key] = value;
  }

  CrossFadeState formState = CrossFadeState.showFirst;

  @override
  Widget build(BuildContext context) {
    // firebaseController.toggleSubmit();
    final FocusNode emailFocusNode = new FocusNode();
    TextEditingController emailController = new TextEditingController();
    final FocusNode passwordFocusNode = new FocusNode();
    TextEditingController passwordController = new TextEditingController();


    return PlatformScaffold(
      body: MenuContainer(
        mainAxisAlignment: MainAxisAlignment.center,
        scrollable: true,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              Icons.pets,
              color: ColorHelper.materialColor.shade500,
              size: 200.w,
            ),
            Text(
              'My Boilerplate',
              style: TypographyHelper.headline6()
                  .copyWith(color: ColorHelper.materialColor.shade500),
            ),
            AnimatedCrossFade(
              duration: const Duration(seconds: 1),
              firstChild: Column(
                children: [
                  FormBox(
                    formKey: _loginFormKey,
                    subtitles: [
                      Text('Please login with your Firebase account',
                              style: TypographyHelper.body1())
                          .align()
                    ],
                    inputFields: [
                      AdaptiveTextField.android(
                        focusNode: emailFocusNode,
                        controller: emailController,
                        autoFocus: true,
                        dataKey: 'email',
                        labelText: "Email",
                        helperText: "",
                        prefix: Icons.person.asIcon(false),
                        keyboardType: TextInputType.emailAddress,
                        validators: [Validation.empty, Validation.email],
                        onSaved: _fillData,
                      ).uiPaddingOnly(b: 20),
                      AdaptiveTextField.android(
                        focusNode: passwordFocusNode,
                        controller: passwordController,
                        obscureText: true,
                        prefix: Icons.lock.asIcon(false),
                        dataKey: 'password',
                        labelText: "Password",
                        helperText: "",
                        validators: [Validation.empty],
                        onSaved: _fillData,
                      ),
                    ],
                    submitButton: GetBuilder<FirebaseController>(
                      builder: (c) => Container(
                        height: 150.h,
                        child: AdaptiveButton(
                          getxController: firebaseController,
                          text: "Login",
                          onPressed: () {
                            if (!_loginFormKey.currentState.validate())
                              return;
                            firebaseController.login(LoginType.Basic, email: emailController.text, password: passwordController.text);
                            // _formKey.currentState.reset();
                          },
                        ),
                      ),
                    ),
                  ).uiPaddingAll(100),
                  InkWell(
                    child: Text('Social Media Login', style: TypographyHelper.body1()),
                    onTap: () => DialogHelper.showAlertDialog(
                        title: "Please choose the following :",
                        content: SocialLogin()),
                  ).uiPaddingOnly(b: 50),
                  InkWell(
                      child: Text("Don't have an account? Register here",
                          style: TypographyHelper.body1()),
                      onTap: () {
                        setState(() {
                          formState = CrossFadeState.showSecond;
                        });
                      })
                ],
              ),
              secondChild: Column(
                children: [
                  FormBox(
                    formKey: _registerFormKey,
                    subtitles: [
                      Text('Please enter your email and password',
                              style: TypographyHelper.body1())
                          .align()
                    ],
                    inputFields: [
                      AdaptiveTextField.android(
                        focusNode: emailFocusNode,
                        controller: emailController,
                        autoFocus: true,
                        dataKey: 'email',
                        labelText: "Email",
                        helperText: "",
                        prefix: Icons.person.asIcon(false),
                        keyboardType: TextInputType.emailAddress,
                        validators: [Validation.empty, Validation.email],
                        onSaved: _fillData,
                      ).uiPaddingOnly(b: 20),
                      AdaptiveTextField.android(
                        focusNode: passwordFocusNode,
                        controller: passwordController,
                        obscureText: true,
                        prefix: Icons.lock.asIcon(false),
                        dataKey: 'password',
                        labelText: "Password",
                        helperText: "",
                        validators: [Validation.empty],
                        onSaved: _fillData,
                      )
                    ],
                    submitButton: GetBuilder<FirebaseController>(
                      builder: (c) => c.isWaitingResponse
                          ? AdaptiveCircularProgressIndicator()
                          : Container(
                              height: 150.h,
                              child: AdaptiveButton(
                                text: "Register",
                                onPressed: () {
                                  if (!_loginFormKey.currentState.validate())
                                    return;
                                  firebaseController.register(emailController.text, passwordController.text);
                                  // firebaseController.login("basic", email: emailController.text, password: passwordController.text);
                                  // _formKey.currentState.reset();
                                },
                              ),
                            ),
                    ),
                  ).uiPaddingAll(100),
                  InkWell(
                      child: Text("Have an account? Login",
                          style: TypographyHelper.body1()),
                      onTap: () {
                        setState(() {
                          formState = CrossFadeState.showFirst;
                        });
                      })
                ],
              ),
              crossFadeState: formState,
            ).forceAsMaterialWidget()
          ],
        ),
      ),
    );
  }
}

class ConstrainedFlexView extends StatelessWidget {
  final Widget child;
  final double minSize;
  final Axis axis;

  const ConstrainedFlexView(this.minSize, {Key key, this.child, this.axis})
      : super(key: key);

  bool get isHz => axis == Axis.horizontal;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (_, constraints) {
        double viewSize = isHz ? constraints.maxWidth : constraints.maxHeight;
        if (viewSize > minSize) return child;
        return SingleChildScrollView(
          physics: NeverScrollableScrollPhysics(),
          scrollDirection: axis ?? Axis.vertical,
          child: ConstrainedBox(
            constraints: BoxConstraints(
                maxHeight: isHz ? double.infinity : minSize,
                maxWidth: isHz ? minSize : double.infinity),
            child: child,
          ),
        );
      },
    );
  }
}
