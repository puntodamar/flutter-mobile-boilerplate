import 'package:boilerplate/helpers/navigation_helper.dart';
import 'package:boilerplate/screens/screen.dart' show Screen, PlatformScreenType;
import 'package:boilerplate/widgets/navigation_drawer/nav_drawer_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:boilerplate/helpers/extensions.dart';



class AdaptiveBottomNavbarScreen extends StatelessWidget {

  static final String routeName = '/screens/adaptive-bottom-navbar-screen';
  final GlobalKey _scaffoldKey = GlobalKey();

  Widget build(BuildContext context) {
    NavigationHelper.navigationKey = _scaffoldKey;

    return Screen(
      screenKey: _scaffoldKey,
      platformScreenType: PlatformScreenType.Adaptive,
      bottomSafeArea: false,
      title: "Bottom Nav",
      actions: [
        PlatformIcons(context).search.asIcon().uiPaddingOnly(r: 20)
      ],
      adaptiveNavDrawer: NavDrawerItem(true),
      bottomNavBarScreens: NavigationHelper.bottomTabScreens(),
      bottomNavBarItems: NavigationHelper.bottomNavBarItems(),
    );
  }
}