import 'dart:async';
import 'package:boilerplate/controllers/auth_controller.dart';
import 'package:boilerplate/controllers/firebase_controller.dart';
import 'package:boilerplate/controllers/form_controller.dart';
import 'package:boilerplate/controllers/image_picker_controller.dart';
import 'package:boilerplate/controllers/nav_drawer_controller.dart';
import 'package:boilerplate/controllers/storage_controller.dart';
import 'package:boilerplate/helpers/color_helper.dart';
import 'package:boilerplate/screens/example/screens/login_screen.dart';
import 'package:boilerplate/screens/example/screens/notification_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:boilerplate/helpers/device_info_helper.dart';
import 'package:boilerplate/helpers/extensions.dart';
import 'adaptive_single_screen.dart';
import 'package:get/get.dart';

class SplashScreenExample extends StatefulWidget {
  static String routeName = "/splash";

  @override
  _SplashScreenExampleState createState() => _SplashScreenExampleState();
}

class _SplashScreenExampleState extends State<SplashScreenExample> {
  NavDrawerController navDrawerController;
  Timer timer;

  @override
  Widget build(BuildContext context) {

    Get.put(AuthController(), permanent: true);
    Get.put(FirebaseController(), permanent: true);
    Get.put(FormController(), permanent: true);
    Get.put(ImagePickerController(), permanent: true);
    Get.put(StorageController(), permanent: true);
    Get.put(FirebaseController(), permanent: true);
    navDrawerController = Get.put(NavDrawerController(), permanent: true);


    DeviceInfoHelper.init(context);

    ScreenUtil.init(context, designSize: Size(1080, 1920), allowFontScaling: false);
    ColorHelper.init();
    return PlatformScaffold(
        body: Container(
            decoration: BoxDecoration(color: Colors.black
//        image: DecorationImage(
//          image: AssetImage(
//            'assets/images/background.png',
//          ),
//          fit: BoxFit.cover,
//        ),
            ),
            // child: AdaptiveSingleScreen(),


            child: GetBuilder<AuthController>(
                builder: (c) {
                  if (c.isAuthenticated) {
                    startTime(AdaptiveSingleScreen.routeName);
                    return splash();
                  }
                  else {
                    return FutureBuilder(
                        future: c.tryAutoLogin(),
                        builder: (ctx, snapshot) {
                          if (snapshot.connectionState ==
                              ConnectionState.waiting) {
                            return splash();
                          }
                          else {
                            c.isAuthenticated ? startTime(AdaptiveSingleScreen.routeName) : startTime(LoginScreen.routeName);
                            // startTime(LoginScreen.routeName);
                            // login screen
                            // timer.cancel();
                            // navDrawerController.currentRouteName =
                            //     AdaptiveSingleScreen.routeName;
                            return splash();
                          }
                        }
                    );
                  }
                })
        )
    );
  }

  Widget splash() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Icon(
            Icons.pets,
            size: 10.0.scl,
            color: Colors.white,
          ),
          SizedBox(
            height: 1.0.scl,
          ),
          Text(
            'My Boilerplate',
            style: TextStyle(
                color: Colors.white,
                fontSize: 2.0.fz,
                decoration: TextDecoration.none),
          )
        ],
      ),
    );
  }

  startTime(String routeName) async {
    var duration = new Duration(seconds: 1);
    timer = new Timer(duration, () => route(routeName));
    return timer;
  }

  route(String routeName) {
    navDrawerController.currentRouteName.value =
        AdaptiveSingleScreen.routeName;
    Get.offAndToNamed(routeName);
  }
}
