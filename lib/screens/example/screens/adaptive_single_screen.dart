import 'package:boilerplate/screens/screen.dart' show Screen, PlatformScreenType;
import 'package:boilerplate/widgets/navigation_drawer/nav_drawer_item.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';

import 'package:boilerplate/helpers/navigation_helper.dart';
import 'package:boilerplate/helpers/extensions.dart';
import 'package:responsive_builder/responsive_builder.dart';

import 'adaptive_single_screen_content.dart';

class AdaptiveSingleScreen extends StatelessWidget {
  static String routeName = '/screens/sub-navigation-1/adaptive-single-screen';
  final GlobalKey _scaffoldKey = GlobalKey();

  Widget build(BuildContext context) {
    NavigationHelper.navigationKey = _scaffoldKey;
    return OrientationBuilder(
      builder: (context, _) {
        return Screen.single(
          platformScreenType: PlatformScreenType.Adaptive,
          screenKey: _scaffoldKey,
          sliverAppBar: true,
          adaptiveNavDrawer: NavDrawerItem(true),
          actions: <Widget>[
            PlatformIcons(context).search.asIcon().uiPaddingOnly(r: 20)
          ],
          title: "Sliver Screen",
          body: ScreenTypeLayout.builder(
            mobile: (BuildContext context) => OrientationLayoutBuilder(
              portrait: (context) => AdaptiveSingleScreenHelper("Portrait"),
              landscape: (context) => AdaptiveSingleScreenHelper("Landscape")),
            tablet: (BuildContext context) => OrientationLayoutBuilder(
              portrait: (context) => AdaptiveSingleScreenHelper("Portrait"),
              landscape: (context) => AdaptiveSingleScreenHelper("Landscape")),
            ),
        );
      },
    );
  }
}
