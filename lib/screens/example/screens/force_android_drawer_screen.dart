import 'package:boilerplate/helpers/device_info_helper.dart';
import 'package:boilerplate/helpers/navigation_helper.dart';
import 'package:boilerplate/helpers/typography_helper.dart';
import 'package:boilerplate/screens/screen.dart' show Screen;
import 'package:boilerplate/widgets/menu_container.dart';
import 'package:boilerplate/widgets/navigation_drawer/nav_drawer_item.dart';
import 'package:flutter/material.dart';

class ForceAndroidDrawerScreen extends StatelessWidget {
  static final String routeName = "/screens/force-android-drawer";
  final GlobalKey _scaffoldKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    NavigationHelper.navigationKey = _scaffoldKey;

    return Screen.withAndroidDrawer(
        screenKey: _scaffoldKey,
        androidNavDrawer: NavDrawerItem(false),
        actions: [
          PopupMenuButton<Choice>(
              itemBuilder: (BuildContext context) {
            return choices.skip(2).map((Choice choice) {
              return PopupMenuItem<Choice>(
                value: choice,
                child: Text(choice.title),
              );
            }).toList();
          })
        ],
        title: "Force Android Drawer",
        body: MenuContainer(
            mainAxisAlignment: MainAxisAlignment.center,
            child: Column(
              children: [
                Text(
                  'This page uses Platform instead of PlatformScaffold',
                  style: TypographyHelper.body1().copyWith(letterSpacing: 0),
                ),
              ],
            )
        )
    );
  }
}

class Choice {
  const Choice({this.title, this.icon});

  final String title;
  final IconData icon;
}

const List<Choice> choices = const <Choice>[
  const Choice(title: 'Car', icon: Icons.directions_car),
  const Choice(title: 'Bicycle', icon: Icons.directions_bike),
  const Choice(title: 'Boat', icon: Icons.directions_boat),
  const Choice(title: 'Bus', icon: Icons.directions_bus),
  const Choice(title: 'Train', icon: Icons.directions_railway),
  const Choice(title: 'Walk', icon: Icons.directions_walk),
];
