import 'package:flutter/material.dart';
import 'adaptive_single_screen_content.dart';

class AdaptiveSingleScreenLandscape extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return AdaptiveSingleScreenHelper("Landscape");
  }
}
