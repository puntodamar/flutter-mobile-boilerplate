import 'package:boilerplate/helpers/navigation_helper.dart';
import 'package:boilerplate/widgets/buttons/adaptive_button.dart';
import 'package:boilerplate/widgets/navigation_drawer/nav_drawer_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class WithoutBoilerplateScreen extends StatelessWidget {

 static String routeName = "/screens/without-boilerplate";

 @override
 Widget build(BuildContext context) {
   final GlobalKey _scaffoldKey = GlobalKey();
   NavigationHelper.navigationKey = _scaffoldKey;

   return Scaffold(
     key: _scaffoldKey,
     drawer: NavDrawerItem(true),
     appBar: AppBar(
       centerTitle: true,
     ),
     body: Container(
       child: Center(
         child: Column(
           mainAxisAlignment: MainAxisAlignment.center,
           children: [
             AdaptiveButton(
               text: "awawa",
             ),
             RaisedButton(
               child: Text('Button', style: TextStyle(fontSize: 30.ssp),),
               onPressed: (){},
             )
           ],
         ),
       ),
     ),
   );
 }
}

