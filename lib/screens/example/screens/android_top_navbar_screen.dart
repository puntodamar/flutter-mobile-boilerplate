
import 'package:boilerplate/controllers/nav_drawer_controller.dart';
import 'package:boilerplate/helpers/navigation_helper.dart';
import 'package:boilerplate/screens/screen.dart' show Screen;
import 'package:boilerplate/widgets/navigation_drawer/nav_drawer_item.dart';
import 'package:flutter/material.dart';
import '../../../helpers/device_info_helper.dart';

class AndroidTopNavbarScreen extends StatelessWidget {
  static String routeName = '/screens/android-top-navbar-screen';
  final GlobalKey _scaffoldKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    NavigationHelper.navigationKey = _scaffoldKey;

    print("top nav");

    return Screen.withMaterialTopNav(
      screenKey: _scaffoldKey,
      androidNavDrawer: NavDrawerItem(true),
      title: "Single Screen Top Tabbed",
      topSafeArea: false,
      topNavBarScreens: NavigationHelper.topAppBarTabScreens,
      topNavBarItems: NavigationHelper.topAppBarTabItems,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: NavigationHelper.floatingActionButton(),
    );
  }
}
