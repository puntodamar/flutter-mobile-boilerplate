import 'package:boilerplate/controllers/firebase_controller.dart';
import 'package:boilerplate/helpers/dialog_helper.dart';
import 'package:boilerplate/helpers/typography_helper.dart';
import 'package:boilerplate/widgets/adaptive_circular_progress_indicator.dart';
import 'package:boilerplate/widgets/forms/adaptive_switch.dart';
import 'package:boilerplate/widgets/buttons/adaptive_button.dart';
import 'package:boilerplate/helpers/shape_helper.dart';
import 'package:boilerplate/widgets/menu_area.dart';
import 'package:boilerplate/widgets/menu_container.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../../widgets/buttons/adaptive_button.dart';
import 'package:boilerplate/helpers/color_helper.dart';
import 'package:boilerplate/helpers/extensions.dart';
import 'package:get/get.dart';

// ignore: must_be_immutable
class AdaptiveSingleScreenHelper extends StatelessWidget {

  final String title;
  AdaptiveSingleScreenHelper(this.title);

  FirebaseController firebaseController = Get.find();

  @override
  Widget build(BuildContext context) {

    return MenuContainer(
      scrollable: true,
      showLogo: true,
      child: MenuArea(
        color: Colors.white,
        children: <Widget>[
          Text(title, style: TypographyHelper.headline3())
              .align(Alignment.centerLeft),
          Text("Hello, ${firebaseController.displayName}", style: TypographyHelper.body2().copyWith(fontWeight: FontWeight.w300))
              .align(Alignment.centerLeft)
              .uiPaddingOnly(b: 30),
          AdaptiveButton.android(
            text: "Android Button",
            prefixIcon: Icons.people.asIcon(),
            suffixIcon: Icons.people_outline.asIcon(),
            onPressed: (){},
          ),
          AdaptiveButton.iOS(
            text: "iOS Button",
            onPressed: (){},
            buttonType: ButtonType.Filled,
          ).uiPaddingOnly(b: 30),
          MenuArea(
            circular: true,
            color: ColorHelper.accentColor,
            children: <Widget>[
              AdaptiveButton(
                text: "Adaptive Outline Button",
                buttonType: ButtonType.Outline,
                shape: ShapeHelper.buttonRoundedRectangle,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  AdaptiveSwitch(
                    onPressed: () => print("On press callback called"),
                  ),
                  AdaptiveCircularProgressIndicator(),
                ],
              ).uiPaddingOnly(t: 30),

              AdaptiveButton(
                  text: 'show alert dialog',
                  onPressed: () => DialogHelper.showAlertDialog(
                      prefixIcon: Icons.warning.asIcon(false))),
              AdaptiveButton(
                text: 'Show modal sheet',
                onPressed: () {
                  DialogHelper.showModalSheet(
                    Text('Modal Bottom Sheet', style: TypographyHelper.body1(),),
                  );
                },
              ),
              AdaptiveButton(
                text: "Show Action Sheet",
                onPressed: () {
                  // showLoadingIndicator(context: context);

                  // DialogHelper.showActionSheet(
                  //   context: context,
                  //     actions: [
                  //       CupertinoActionSheetAction(
                  //         child: Text('Action'),
                  //         onPressed: () {},
                  //       )
                  //     ]);
                },
              ),
              AdaptiveButton(
                text: "Show Error Sheet",
                onPressed: () {
                  DialogHelper.showBottomSheetErrorDialog("Wawawawa");
                },
              )
            ],
          ).uiPaddingOnly(b: 20),
        ],
      ),
    );

  }
// OverlayEntry _loaderEntry;
//
// OverlayEntry createOverlayEntry() {
//   return OverlayEntry(
//     builder: (context) {
//       return Material(
//         color: const Color(0x00000000),
//         child: SafeArea(
//           child: Stack(
//             alignment: Alignment.center,
//             children: [
//               Container(
//                 color: Colors.transparent.withOpacity(0.3),
//               ),
//               Container(
//                 padding: const EdgeInsets.all(16.0),
//                 decoration: BoxDecoration(
//                   color: Colors.black38,
//                   borderRadius: BorderRadius.circular(10.0),
//                 ),
//                 child: Column(
//                   mainAxisSize: MainAxisSize.min,
//                   children: [
//                     const Text('Loading ...'),
//                     const SizedBox(
//                       height: 8.0,
//                     ),
//                     const CircularProgressIndicator(),
//                   ],
//                 ),
//               ),
//             ],
//           ),
//         ),
//       );
//     },
//   );
// }
//
//
// void showLoadingIndicator({@required BuildContext context}) {
//   _loaderEntry = createOverlayEntry();
//   Overlay.of(context).insert(_loaderEntry);
// }
//
// void hideLoadingIndicator() {
//   _loaderEntry.remove();
// }



}
