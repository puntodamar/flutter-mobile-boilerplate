import 'package:boilerplate/helpers/color_helper.dart';
import 'package:boilerplate/helpers/device_info_helper.dart';
import 'package:boilerplate/helpers/navigation_helper.dart';
import 'package:boilerplate/screens/screen.dart' show Screen;
import 'package:boilerplate/widgets/navigation_drawer/nav_drawer_item.dart';
import 'package:boilerplate/widgets/forms/toggle_tab.dart';
import 'package:boilerplate/widgets/menu_area.dart';
import 'package:flutter/material.dart';

import 'package:boilerplate/helpers/extensions.dart';

enum SingingCharacter { lafayette, jefferson }

class RadioButtonScreen extends StatefulWidget {
  static final routeName = "/form/radio-button";

  @override
  _RadioButtonScreenState createState() => _RadioButtonScreenState();
}

class _RadioButtonScreenState extends State<RadioButtonScreen> {
  SingingCharacter _character = SingingCharacter.lafayette;
  final GlobalKey _scaffoldKey = GlobalKey();
  List<bool> checkBoxValues = [false, false, true];

  @override
  Widget build(BuildContext context) {
    NavigationHelper.navigationKey = _scaffoldKey;

    return Screen.single(
      screenKey: _scaffoldKey,
      adaptiveNavDrawer: NavDrawerItem(true),
      title: "Radio & Checkbox Buttons",
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            MenuArea(
              title: "Radio Buttons",
              color: Colors.white,
              children: <Widget>[
                RadioListTile<SingingCharacter>(
                  secondary: Icons.people.asIcon(false),
                  subtitle: Text('Subtitle'),
                  activeColor: ColorHelper.materialColor.shade500,
                  title: const Text('Lafayette'),
                  value: SingingCharacter.lafayette,
                  groupValue: _character,
                  onChanged: (SingingCharacter value) {
                    setState(() {
                      _character = value;
                    });
                  },
                ).formInputWrapTheme(),
                RadioListTile<SingingCharacter>(
                  activeColor: ColorHelper.materialColor.shade500,
                  title: const Text('Jefferson'),
                  value: SingingCharacter.jefferson,
                  groupValue: _character,
                  onChanged: (SingingCharacter value) {
                    setState(() {
                      _character = value;
                    });
                  },
                ).formInputWrapTheme()
              ],
            ),
            MenuArea(
              title: "Check Boxes",
              color: Colors.white,
              children: <Widget>[
                CheckboxListTile(
                  selected: checkBoxValues[0],
                  activeColor: ColorHelper.materialColor.shade500,
                  title: Text('1'),
                  secondary: Icons.beach_access.asIcon(false),
                  controlAffinity: ListTileControlAffinity.leading,
                  value: checkBoxValues[0],
                  onChanged: (bool value) => setState(() {
                    checkBoxValues[0] = value;
                  }),
                ).formInputWrapTheme(),
                CheckboxListTile(
                  activeColor: ColorHelper.materialColor.shade500,
                  title: Text('2'),
//                  secondary: Icons.beach_access.asIcon(false),
                  controlAffinity: ListTileControlAffinity.platform,
                  value: checkBoxValues[1],
                  onChanged: (bool value) => setState(() {
                    checkBoxValues[1] = value;
                  }),
                ).formInputWrapTheme(),
                CheckboxListTile(
                  activeColor: ColorHelper.materialColor.shade500,
                  title: Text('3'),
                  secondary: Icons.beach_access.asIcon(false),
                  controlAffinity: ListTileControlAffinity.trailing,
                  value: checkBoxValues[2],
                  onChanged: (bool value) => setState(() {
                    checkBoxValues[2] = value;
                  }),
                ).formInputWrapTheme()
              ],
            ),
            MenuArea(
              title: "Toggle Tab",
              color: Colors.white,
              children: <Widget>[
                ToggleTab(
                  labelText: ["Pagi", "Siang"],
                  width: 5.scl,
//                labelIcons: [Icons.beach_access, Icons.people, Icons.android],
                ),
                ToggleTab(
                  labelText: ["", ""],
                  labelIcons: [Icons.person, Icons.pregnant_woman],
                  width: 5.scl,
                ),
                ToggleTab(
                  labelText: ["Merah (10)", "Kuning (0)", "Biru (3)"],
                  labelIcons: [Icons.beach_access, Icons.people, Icons.android],
                )
              ],
            )
          ],
        ).forceAsMaterialWidget(),
      ),
    );
  }
}