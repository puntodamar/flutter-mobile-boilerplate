import 'package:boilerplate/helpers/navigation_helper.dart';
import 'package:boilerplate/helpers/typography_helper.dart';
import 'package:boilerplate/screens/screen.dart' show Screen;
import 'package:boilerplate/widgets/forms/adaptive_slider.dart';
import 'package:boilerplate/widgets/menu_container.dart';
import 'package:boilerplate/widgets/navigation_drawer/nav_drawer_item.dart';
import 'package:flutter/material.dart';
import 'package:boilerplate/helpers/extensions.dart';

class SliderScreen extends StatelessWidget {
  static final String routeName = "/form/slider";
  final GlobalKey _scaffoldKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    NavigationHelper.navigationKey = _scaffoldKey;

    return Screen.single(
      screenKey: _scaffoldKey,
      adaptiveNavDrawer: NavDrawerItem(true),
      title: "Sliders",
      body: MenuContainer(
          child: Column(
            children: <Widget>[
              Text('Adaptive Slider', style: TypographyHelper.body1(),),
              AdaptiveSlider(),
              Text('Android Slider with prefix & suffix', style: TypographyHelper.body1(),),
              AdaptiveSlider.android(
                useLabel: true,
                labelText: "Label",
                divison: 5,
//                backgroundColor: ColorHelper.materialColor.shade50,
                prefix: Icons.volume_mute.asIcon(),
                suffix: Icons.volume_up.asIcon(false),
              ),
              Text('Range Slider', style: TypographyHelper.body1(),),
              AdaptiveSlider.range(
                  max: 100,
                  min: 0,
                  rangeValues: RangeValues(20, 50)
              ),

            ],
          ).uiPaddingAll(20)
      ),
    );
  }
}

