import 'package:boilerplate/helpers/navigation_helper.dart';
import 'package:boilerplate/helpers/typography_helper.dart';
import 'package:boilerplate/screens/screen.dart' show Screen, PlatformScreenType;
import 'package:boilerplate/widgets/buttons/adaptive_button.dart';
import 'package:boilerplate/widgets/menu_container.dart';
import 'package:boilerplate/widgets/navigation_drawer/nav_drawer_item.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AutoImplyLeadingScreen extends StatelessWidget {
  static final String routeName = "/screens/autoimply";
  final GlobalKey _scaffoldKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    NavigationHelper.navigationKey = _scaffoldKey;

    return Screen.single(
        screenKey: _scaffoldKey,
        platformScreenType: PlatformScreenType.Adaptive,
        adaptiveNavDrawer: NavDrawerItem(true),
        title: "Auto-imply Leading",
        body: MenuContainer(
          mainAxisAlignment: MainAxisAlignment.center,
          child: Container(
              child: Column(
                children: [
                  Text('Page 1', style: TypographyHelper.headline6()),
                  AdaptiveButton(
                    text: "Go to Page 2",
                    onPressed: () => Get.toNamed(AutoImplyDestinationScreen.routeName),
                  ),
                ],
              )
          ),
        )
    );
  }
}

class AutoImplyDestinationScreen extends StatelessWidget {
  static final String routeName = "/screens/autoimply-destination";
  final GlobalKey _scaffoldKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Screen.single(
        screenKey: _scaffoldKey,
        title: "Auto-imply Leading Destination",
        body: MenuContainer(
          mainAxisAlignment: MainAxisAlignment.center,
          child: Text('Page 2', style: TypographyHelper.headline6(),),
        )
    );
  }
}
