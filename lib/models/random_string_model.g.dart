// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'random_string_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RandomStringModel _$RandomStringModelFromJson(Map<String, dynamic> json) {
  return RandomStringModel(
    json['randomString'] as String,
    json['dateTime'] == null
        ? null
        : DateTime.parse(json['dateTime'] as String),
    json['key'] as String,
  );
}

Map<String, dynamic> _$RandomStringModelToJson(RandomStringModel instance) =>
    <String, dynamic>{
      'key': instance.key,
      'randomString': instance.randomString,
      'dateTime': instance.dateTime?.toIso8601String(),
    };
