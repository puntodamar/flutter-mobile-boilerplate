import 'package:firebase_database/firebase_database.dart';
import 'package:json_annotation/json_annotation.dart';

part 'random_string_model.g.dart';

@JsonSerializable()
class RandomStringModel {
  String key;
  String randomString;
  DateTime dateTime;

  RandomStringModel(this.randomString, this.dateTime, [this.key]);


  RandomStringModel.map(dynamic obj) {
    this.key = obj['key'];
    this.randomString = obj['randomString'];
    this.dateTime = DateTime.parse(obj['dateTime']);
  }

  RandomStringModel.fromSnapshot(DataSnapshot snapshot) {
    this.key = snapshot.key;
    this.randomString = snapshot.value['randomString'];
    this.dateTime = DateTime.parse(snapshot.value['dateTime']);
  }

}
