import 'package:boilerplate/helpers/color_helper.dart';
import 'package:boilerplate/helpers/typography_helper.dart';
import 'package:flutter/material.dart';
import 'package:boilerplate/helpers/constants.dart';
import 'package:boilerplate/helpers/extensions.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class MenuContainer extends StatelessWidget {
  final Widget child;
  final bool showLogo;
  final String title;
  final Widget additional;
  final bool scrollable;
  final MainAxisAlignment mainAxisAlignment;
  final double screenFraction;
  final bool center;
  final double height;

  MenuContainer({@required this.child,
    this.title,
    this.additional,
    this.showLogo = false,
    this.scrollable = false,
    this.screenFraction = 1,
    this.center = false,
    this.height,
    this.mainAxisAlignment = MainAxisAlignment.start});

  @override
  Widget build(BuildContext context) {

    Widget menuContainer = SizedBox(
      height: height ?? 1.sh,
      child: Column(
        // mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: mainAxisAlignment,
        children: [
          Container(
              width: double.infinity,
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    if(showLogo) logoImage2 == null
                        ? Placeholder(
                      color: ColorHelper.materialColor.shade500,).boxed(
                      height: 200.w, width: 200.w,).uiPaddingOnly(l: 20, t: 20)
                        : Image
                        .asset(
                      logoImage2, height: 200.w,
                      width: 200.w,
                      fit: BoxFit.fill,),
                    if(title != null) Text(
                      title,
                      style: TypographyHelper.headline6(),
                    ),
                  ]
              )
          ),
          if (additional != null) SizedBox(height: 30.h),
          if (additional != null) additional,
          child
        ],
      ),
    );

    if(scrollable != null) menuContainer = menuContainer.scrollable(scrollable);

    if (center) menuContainer = menuContainer.center();
return menuContainer.uiPaddingAll(30);
    // return FractionallySizedBox(
    //   heightFactor: screenFraction,
    //   child: menuContainer,
    // );
  }
}
