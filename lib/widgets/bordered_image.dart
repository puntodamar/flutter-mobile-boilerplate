import 'package:boilerplate/helpers/color_helper.dart';
import 'package:flutter/material.dart';

class BorderedImage extends StatelessWidget {

  final String url;
  final Color color;

  double radius;
  BoxShape shape;

  BorderedImage.circle({
    this.url, this.color, this.radius}){
    shape = BoxShape.circle;
    radius = 50;
  }

  BorderedImage.rectangle({this.color, this.url}){
    shape = BoxShape.rectangle;
  }

  @override
  Widget build(BuildContext context) {

    Widget image;
    if(shape == BoxShape.circle)
      image = CircleAvatar(
        radius: radius,
        backgroundImage: NetworkImage(url),
      );
    else image = Image.network(url);


    return Container(
      decoration: BoxDecoration(
          shape: shape,
          border: new Border.all(
            color: (color != null) ? color : ColorHelper.materialColor.shade500 ,
            width: 4.0,
          )),
      child: image,
    );
  }
}
