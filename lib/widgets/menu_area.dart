import 'package:boilerplate/helpers/color_helper.dart';
import 'package:boilerplate/helpers/typography_helper.dart';
import 'package:flutter/material.dart';
import 'package:boilerplate/helpers/extensions.dart';

enum MenuAreaBoxDecorationType { Default, Circular, Vertical, Horizontal }

class MenuArea extends StatelessWidget {
  final List<Widget> children;
  final bool borderlessBottom;
  final bool noBorder;
  final Color color;
  final bool circular;
  final String title;

  MenuArea({
    this.children,
    this.title,
    this.color,
    this.borderlessBottom = false,
    this.noBorder = false,
    this.circular = false,
  });

  @override
  Widget build(BuildContext context) {
    double border = 3.scl;
    Color bgColor = color == null ? ColorHelper.accentColor : color;
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
          color: bgColor,
          borderRadius: new BorderRadius.only(
              topLeft: const Radius.circular(20.0),
              topRight: const Radius.circular(20.0),
              bottomLeft: Radius.circular(circular ? 20 : 0),
              bottomRight: Radius.circular(circular ? 20 : 0))),
      child: Container(
        margin: noBorder
            ? null
            : EdgeInsets.fromLTRB(
                border, border, border, borderlessBottom ? 0 : border),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if(title != null) Text(title, style: TypographyHelper.headline6(),),
            ...children],
        ),
      ),
    );
  }
}
