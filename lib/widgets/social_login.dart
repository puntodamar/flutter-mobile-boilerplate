import 'package:boilerplate/controllers/firebase_controller.dart';
import 'package:boilerplate/helpers/dialog_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SocialLogin extends StatelessWidget {

  FirebaseController firebaseController = Get.find();

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Center(
        child: Wrap(
          direction: Axis.horizontal,
          spacing: 20,
          runSpacing: 20,
          runAlignment: WrapAlignment.spaceBetween,
          children: [
            if(GetPlatform.isAndroid) InkWell(
              child: Image.asset("assets/icons/google.png", width: 50,),
              onTap: () {
                Navigator.of(context, rootNavigator: true).pop();
                firebaseController.login(LoginType.Google);
              },
            ),
            InkWell(
              child: Image.asset("assets/icons/facebook.png", width: 50,),
              onTap: () => print(Get.isDialogOpen),
            ),
            InkWell(
              child: Image.asset("assets/icons/twitter.png", width: 50,),
              onTap: (){},
            ),
            InkWell(
              child: Image.asset("assets/icons/github.png", width: 50,),
              onTap: (){},
            ),
            if(GetPlatform.isAndroid) InkWell(
              child: Image.asset("assets/icons/google.png", width: 50,),
              onTap: (){},
            ),
            InkWell(
              child: Image.asset("assets/icons/facebook.png", width: 50,),
              onTap: (){},
            ),
            InkWell(
              child: Image.asset("assets/icons/twitter.png", width: 50,),
              onTap: (){},
            ),
            InkWell(
              child: Image.asset("assets/icons/github.png", width: 50,),
              onTap: (){},
            ),
            // Row(
            //   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            //   children: [
            //     InkWell(
            //       child: Image.asset("assets/icons/google.png", width: 50,),
            //       onTap: (){},
            //     ),
            //     InkWell(
            //       child: Image.asset("assets/icons/facebook.png", width: 50,),
            //       onTap: (){},
            //     ),
            //     InkWell(
            //       child: Image.asset("assets/icons/twitter.png", width: 50,),
            //       onTap: (){},
            //     ),
            //     InkWell(
            //       child: Image.asset("assets/icons/github.png", width: 50,),
            //       onTap: (){},
            //     ),
            //     InkWell(
            //       child: Image.asset("assets/icons/google.png", width: 50,),
            //       onTap: (){},
            //     ),
            //     InkWell(
            //       child: Image.asset("assets/icons/facebook.png", width: 50,),
            //       onTap: (){},
            //     ),
            //     InkWell(
            //       child: Image.asset("assets/icons/twitter.png", width: 50,),
            //       onTap: (){},
            //     ),
            //     InkWell(
            //       child: Image.asset("assets/icons/github.png", width: 50,),
            //       onTap: (){},
            //     ),
            //   ],
            // )
          ],
        ),
      ),
    );
  }
}
