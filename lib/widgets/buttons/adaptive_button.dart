import 'dart:io' show Platform;

import 'package:boilerplate/helpers/color_helper.dart';
import 'package:boilerplate/helpers/extensions.dart';
import 'package:boilerplate/helpers/shape_helper.dart';
import 'package:boilerplate/helpers/typography_helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:recase/recase.dart';

enum PlatformButtonType { iOS, Android, Adaptive }
enum ButtonType { Raised, Flat, Outline, Filled }

double globalwidth;
String globaltext;
TextAlign globaltextAlign;
Function globalonPressed;
double globalheight;
Image globalsuffixImage;
Icon globalsuffixIcon;
Image globalprefixImage;
Icon globalprefixIcon;
TextStyle globaltextStyle;
ShapeBorder globalshape;
MainAxisAlignment globalmainAxisAlignment;
Color globaltextColor;
ButtonType globalbuttonType;
Color globalborderColor;
Color globalcolor;
GetxController globalgetxController;
PlatformButtonType globalPlatformButtonType;

class AdaptiveButton extends StatelessWidget {
  final double width;
  final String text;
  final TextAlign textAlign;
  final Function onPressed;
  final double height;
  Image suffixImage;
  Icon suffixIcon;
  Image prefixImage;
  Icon prefixIcon;
  final TextStyle textStyle;
  final MainAxisAlignment mainAxisAlignment;
  final Color textColor;
  ShapeBorder shape;
  ButtonType buttonType;
  Color borderColor;
  PlatformButtonType platformButtonType;
  Color color;
  GetxController getxController;

  AdaptiveButton({
    @required this.text,
    this.onPressed,
    this.prefixIcon,
    this.prefixImage,
    this.suffixIcon,
    this.suffixImage,
    this.textStyle,
    this.shape,
    this.textColor,
    this.borderColor,
    this.color,
    this.getxController,
    this.width = 0,
    this.height = 0,
    this.textAlign = TextAlign.start,
    this.buttonType = ButtonType.Flat,
    this.mainAxisAlignment = MainAxisAlignment.spaceAround,
  }){
    this.platformButtonType = PlatformButtonType.Adaptive;
  }

  AdaptiveButton.outline({
    @required this.text,
    this.textAlign,
    this.onPressed,
    this.suffixImage,
    this.suffixIcon,
    this.prefixImage,
    this.prefixIcon,
    this.textStyle,
    this.shape,
    this.textColor,
    this.getxController,
    this.borderColor,
    this.height = 0,
    this.width = 0,
    this.mainAxisAlignment = MainAxisAlignment.spaceAround,
  }){
    this.platformButtonType = PlatformButtonType.Adaptive;
    this.buttonType = ButtonType.Outline;
  }

  AdaptiveButton.android({
    @required this.text,
    this.textAlign,
    this.onPressed,
    this.suffixImage,
    this.suffixIcon,
    this.prefixImage,
    this.prefixIcon,
    this.textStyle,
    this.shape,
    this.textColor,
    this.getxController,
    this.borderColor,
    this.height = 0,
    this.width = 0,
    this.buttonType = ButtonType.Raised,
    this.mainAxisAlignment = MainAxisAlignment.spaceAround,
}){
    this.platformButtonType = PlatformButtonType.Android;
}

  AdaptiveButton.iOS({
    @required this.text,
    this.width = 0,
    this.textAlign,
    this.onPressed,
    this.height = 0,
    this.textStyle,
    this.textColor,
    this.getxController,
    this.buttonType,
    this.mainAxisAlignment = MainAxisAlignment.spaceAround,
}){
    this.platformButtonType = PlatformButtonType.iOS;
  }

  @override
  Widget build(BuildContext context) {
    globalwidth = width;
    globaltext = text;
    globaltextAlign = textAlign;
    globalonPressed = onPressed;
    globalheight = height;
    globalsuffixImage = suffixImage;
    globalsuffixIcon = suffixIcon;
    globalprefixImage = prefixImage;
    globalprefixIcon = prefixIcon;
    globaltextStyle = textStyle;
    globalshape = shape;
    globalmainAxisAlignment = mainAxisAlignment;
    globaltextColor = textColor;
    globalbuttonType = buttonType;
    globalborderColor = borderColor;
    globalcolor = color;
    globalgetxController = getxController;
    globalPlatformButtonType = platformButtonType;

    Widget button;
    Widget child;

    // set child data
    if (globalwidth != 0 || globalheight != 0) {
      child = (getxController != null)
          ? AdaptiveButtonLoading()
              .boxed(width: globalwidth, height: globalheight)
          : StaticButtonContent()
              .boxed(width: globalwidth, height: globalheight);
    } else {
      child = (getxController != null)
          ? AdaptiveButtonLoading()
          : StaticButtonContent();
    }


    // set button
    if (buttonType == ButtonType.Outline) {
      button = SizedBox(
        height: 80.h,
        child: OutlineButton(
          shape: shape ?? ShapeHelper.buttonRoundedRectangle,
          borderSide: BorderSide(
            color: ColorHelper.platformAppBarBackgroundColor,
            style: BorderStyle.solid,
            width: 2.5,
          ),
          onPressed: onPressed ?? () => print("Button '$text' pressed"),
          child: child,
        ),
      );

    }

    else if (platformButtonType == PlatformButtonType.Android) {
      if (buttonType == ButtonType.Flat) {
        button = FlatButton(
            onPressed: onPressed ?? () => print("Button '$text' pressed"),
            child: child);
      } else
        button = RaisedButton(
          onPressed: onPressed ?? () => print("Button '$text' pressed"),
          child: child,
        );
    }

    else if (platformButtonType == PlatformButtonType.iOS) {
      if(buttonType == ButtonType.Filled) {
        button = CupertinoButton.filled(
          onPressed: onPressed,
          child: child,
          disabledColor: ColorHelper.materialColor.shade200,
        );

      }
      else {
        button = CupertinoButton(
          onPressed: null,
          child: child,
          disabledColor: ColorHelper.materialColor.shade200,
        );
      }
    }

    else if (platformButtonType == PlatformButtonType.Adaptive) {
      button = PlatformButton(
        material: (_, __) => buttonType == ButtonType.Raised
            ? ColorHelper.materialRaisedButtonData(color)
            : null,
        materialFlat: (_, __) => buttonType == ButtonType.Flat
            ? ColorHelper.materialFlatButtonData(color)
            : null,

        cupertino: (_, __) => buttonType == ButtonType.Flat
            ? ColorHelper.cupertinoButtonData(color)
            : null,
        // cupertinoFilled: (_, __) => buttonType == ButtonType.Raised
        //     ? ColorHelper.cupertinoFilledButtonData()
        //     : null,
        onPressed: onPressed == null
            ? () => print("Button '$text' pressed")
            : onPressed,
        child: child,
      );
    }

    if (Platform.isIOS)
      button = button.uiPaddingOnly(t: 30);
    else
      button = button.uiPaddingOnly(t: 20);
    return button;
  }
}

class StaticButtonContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: globalmainAxisAlignment,
      children: <Widget>[
        if (globalprefixIcon != null) globalprefixIcon,
        if (globalprefixImage != null) globalprefixImage,
        if ((globalprefixIcon != null || globalprefixImage != null) &&
            globalmainAxisAlignment != MainAxisAlignment.center)
          const Spacer(),
        ButtonText(),
        if ((globalsuffixIcon != null || globalsuffixIcon != null) &&
            globalmainAxisAlignment != MainAxisAlignment.center)
          const Spacer(),
        if (globalsuffixIcon != null) globalsuffixIcon,
        if (globalsuffixImage != null) globalsuffixImage,
      ],
    );
  }
}

// ignore: must_be_immutable
class AdaptiveButtonLoading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
      init: globalgetxController,
      builder: (c) {
        if (c.isWaitingResponse)
          return Row(
            mainAxisAlignment: globalmainAxisAlignment,
            children: [
              Spacer(),
              GetPlatform.isAndroid
                  ? CircularProgressIndicator()
                  : CupertinoActivityIndicator(),
              Spacer()
            ],
          );
        else
          return StaticButtonContent();
      },
    );
  }
}

class ButtonText extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    TextStyle textStyle;

    if(globalPlatformButtonType ==  PlatformButtonType.iOS){
      textStyle = (globalbuttonType == ButtonType.Filled) ? TypographyHelper.button() : TypographyHelper.button().copyWith(color: ColorHelper.materialColor.shade900);
    }
    else if (globalbuttonType == ButtonType.Outline)
      textStyle = TypographyHelper.button()
          .copyWith(color: ColorHelper.materialColor.shade900);
    else
      textStyle = TypographyHelper.button();


    return Text(
      globaltext.titleCase,
      textAlign: globaltextAlign,
      style: textStyle,
    );
  }
}
