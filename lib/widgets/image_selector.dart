import 'package:boilerplate/controllers/image_picker_controller.dart';
import 'package:boilerplate/helpers/typography_helper.dart';
import 'package:flutter/material.dart';
import 'package:boilerplate/helpers/extensions.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

class ImageSelector extends StatelessWidget {
  final bool camera;
  final bool gallery;
  final ImagePickerController  imagePickerController = Get.put(ImagePickerController());

  ImageSelector({this.camera, this.gallery});

  @override
  Widget build(BuildContext context) {

    return Container(
      height: 150.h,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          if (camera != null)
            InkWell(
              child: Column(
                children: [
                  Icons.camera_alt.asIcon(false),
                  Text(
                    'Camera',
                    style: TypographyHelper.body1(),
                  )
                ],
              ),
              onTap: () {
                Navigator.of(context, rootNavigator: true).pop();
                imagePickerController.getImage(ImageSource.camera);
              },
            ),
          if (gallery != null)
            InkWell(
              child: Column(
                children: [
                  Icons.image.asIcon(false),
                  Text(
                    'Gallery',
                    style: TypographyHelper.body1(),
                  ),
                ],
              ),
              onTap: () {
                Navigator.of(context, rootNavigator: true).pop();
                imagePickerController.getImage(ImageSource.gallery);
              },

            )
        ],
      ),
    );
  }
}
