import 'package:boilerplate/helpers/typography_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

enum DismissableBackgroundPosition {
  Left, Right
}
class DismissibleBackground extends StatelessWidget {

  final Icon icon;
  final String text;
  final Color color;
  final DismissableBackgroundPosition position;
  DismissibleBackground({@required this.text, @required this.position, this.icon, this.color = Colors.green, });

  @override
  Widget build(BuildContext context) {

    Text textWidget = Text(
      text,
      style: TypographyHelper.button(),
      textAlign: position  == DismissableBackgroundPosition.Left ? TextAlign.left : TextAlign.right,
    );

    SizedBox sizebox = SizedBox(width: 150.h,);

    return Container(
      color: color,
      child: Align(
        child: Row(
          mainAxisAlignment: position == DismissableBackgroundPosition.Left ? MainAxisAlignment.start : MainAxisAlignment.end,
          children: <Widget>[
            (position == DismissableBackgroundPosition.Left) ? sizebox : textWidget,
            if(icon != null) icon,
            (position == DismissableBackgroundPosition.Left) ? textWidget : sizebox,
          ],
        ),
        alignment: position == DismissableBackgroundPosition.Left ? Alignment.centerLeft : Alignment.centerRight,
      ),
    );
  }
}
