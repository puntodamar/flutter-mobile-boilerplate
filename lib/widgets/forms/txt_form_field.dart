import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:boilerplate/helpers/extensions.dart';
import 'package:boilerplate/helpers/validation_helper.dart';
import 'dart:io' show Platform;

class TxtFormField extends StatelessWidget {
  final String labelText;
  final String hintText;
  final bool enabled;
  final Icon suffixIcon;
  final Icon prefixIcon;
  final FocusNode focusNode;
  final FocusNode nextField;
  final Function onSaved;
  final String dataKey;
  final bool autoFocus;
  final List <Validation> validationList;
  final String initialValue;
  final int maxLength;
  final TextEditingController controller;

  TxtFormField({
    @required this.dataKey,
    this.labelText,
    this.hintText,
    this.validationList,
    this.suffixIcon,
    this.prefixIcon,
    this.nextField,
    this.focusNode,
    this.onSaved,
    this.enabled = true,
    this.autoFocus = false,
    this.initialValue = "",
    this.maxLength = 1,
    this.controller
  });

  @override
  Widget build(BuildContext context) {
    TextEditingController _controller;

    Widget _androidButton(){
      return TextFormField(
        controller: _controller,
        enabled: enabled,
        focusNode: focusNode,
        initialValue: initialValue,
        autofocus: autoFocus,
//      maxLength: maxLength,
        decoration: InputDecoration(
          prefixIcon: prefixIcon,
          suffixIcon: suffixIcon,
          labelText: labelText,
          hintText: hintText,
          floatingLabelBehavior: FloatingLabelBehavior.never,
          labelStyle: TextStyle(
              color: Theme
                  .of(context)
                  .accentColor,
              fontWeight: FontWeight.bold,
              fontSize: enabled ? 1.4.fz : 2.fz),

        ),
        validator: (value) {
          if (validationList.contains(Validation.empty)){
            var empty = ValidationHelper.isEmpty(value, labelText);
            return empty;
          }
          if (validationList.contains(Validation.email))
            return ValidationHelper.validateEmail(value, labelText);
          return null;
        },
        onFieldSubmitted: (String value) {
          if (nextField != null) FocusScope.of(context).requestFocus(nextField);
        },
        onSaved: (String value) {
          onSaved(dataKey, value);
        },
      );
    }

    Widget _iosButton(){
      return CupertinoTextField(
        controller: controller,
        placeholder: hintText,
        enabled: enabled,
        autofocus: autoFocus,
        focusNode: focusNode,
        onSubmitted: (String value) {
          if (nextField != null) FocusScope.of(context).requestFocus(nextField);
        },
      );
    }

    return _androidButton();
//    return Platform.isIOS ? _iosButton() : _androidButton();
  }
}

