import 'package:flutter/material.dart';
import 'package:boilerplate/widgets/buttons/adaptive_button.dart';

class SubmitButton extends StatelessWidget {
  final String text;
  final Function onPressed;

  SubmitButton({@required this.text, @required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return  Align(
      alignment: Alignment.center,
      child: AdaptiveButton(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        text: text,
        textAlign: TextAlign.center,
        onPressed: onPressed,
      ),
    );
  }
}
