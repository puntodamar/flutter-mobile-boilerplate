import 'package:boilerplate/helpers/color_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_toggle_tab/flutter_toggle_tab.dart';

import 'package:boilerplate/helpers/extensions.dart';
class ToggleTab extends StatefulWidget {

  final List<String> labelText;
  final List<IconData> labelIcons;
  final double width;

  ToggleTab({
    @required this.labelText,
    this.labelIcons,
    this.width = 85
});

  @override
  _ToggleTabState createState() => _ToggleTabState();
}


class _ToggleTabState extends State<ToggleTab> {
  @override
  Widget build(BuildContext context) {
    return FlutterToggleTab(
      // width in percent, to set full width just set to 100
      width: widget.width,
      borderRadius: 30,
//      height: 50,
      initialLabelIndex: 0,
      unSelectedBackgroundColor: ColorHelper.accentColor,
      selectedBackgroundColor: ColorHelper.materialColor.shade500,
      selectedTextStyle: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.w700
      ),
      unSelectedTextStyle: TextStyle(
          color: ColorHelper.materialColor.shade900,
//          fontSize: 14,
      ),
      labels: widget.labelText,
      icons: widget.labelIcons,
      selectedLabelIndex: (index) {
        setState(() {
          print("Selected Index $index");
        });
      },
    ).uiPaddingOnly(t: 20).align();
  }
}
