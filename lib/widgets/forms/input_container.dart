import 'package:flutter/material.dart';

class InputContainer extends StatelessWidget {

  final String title;
  final Widget child;
  InputContainer({@required this.title, @required this.child});

  @override
  Widget build(BuildContext context) {
    return Flex(
      direction: Axis.vertical,
      children: <Widget>[
        Flexible(
          fit: FlexFit.tight,
          child: Container(
            width: double.infinity,
            margin: EdgeInsets.all(30),
            child: child,
          ),
        )
        ],
    );
  }
}
