import 'package:boilerplate/helpers/color_helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';

enum PlatformSwitchType {
  iOS, Android, Adaptive
}

class AdaptiveSwitch extends StatefulWidget {

  final bool initialValue;
  final Function onPressed;
  PlatformSwitchType platformSwitchType;
  bool currentValue;


  AdaptiveSwitch({this.initialValue = true, this.onPressed}){
    this.platformSwitchType = PlatformSwitchType.Adaptive;
  }

  AdaptiveSwitch.android({this.initialValue = true, this.onPressed}){
    this.platformSwitchType = PlatformSwitchType.Android;
  }

  AdaptiveSwitch.iOS({this.initialValue = true, this.onPressed}){
    this.platformSwitchType = PlatformSwitchType.iOS;
  }

  @override
  _AdaptiveSwitchState createState() => _AdaptiveSwitchState(initialValue);
}

class _AdaptiveSwitchState extends State<AdaptiveSwitch> {

  bool currentValue;
  _AdaptiveSwitchState(this.currentValue);

  @override
  Widget build(BuildContext context) {

    if(widget.platformSwitchType == PlatformSwitchType.Adaptive){
      return PlatformSwitch(
        onChanged: (bool value) {
          setState(() {
            currentValue = value;
            widget.currentValue = currentValue;
            if (widget.onPressed == null){
              currentValue ? print("Switch turned on") : print("Switch turned off");
            }
            else widget.onPressed();
          });
        },
        value: currentValue,
        material: (_, __)  => ColorHelper.materialSwitchData(),
        cupertino: (_,__) => ColorHelper.cupertinoSwitchData(),
      );
    }
    else if(widget.platformSwitchType == PlatformSwitchType.Android){
      return Switch(
        onChanged: (bool value) {
          setState(() {
            currentValue = value;
            widget.currentValue = currentValue;
            if (widget.onPressed == null){
              currentValue ? print("Android switch turned on") : print("Android switch turned off");
            }
            else widget.onPressed();
          });
        },
        value: currentValue,
      );
    }

    else if(widget.platformSwitchType == PlatformSwitchType.iOS) {
      CupertinoSwitchData themeData = ColorHelper.cupertinoSwitchData();
      return CupertinoSwitch(
        activeColor: themeData.activeColor,
        trackColor: themeData.activeColor,
        onChanged: (bool value) {
          setState(() {
            currentValue = value;
            widget.currentValue = currentValue;
            if (widget.onPressed == null){
              currentValue ? print("Cupertino switch turned on") : print("Cupertino switch turned off");
            }
            else widget.onPressed();
          });
        },
        value: currentValue,
      );
    }

    return Container();


  }
}
