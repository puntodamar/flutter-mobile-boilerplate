import 'package:boilerplate/helpers/color_helper.dart';
import 'package:boilerplate/helpers/typography_helper.dart';
import 'package:flutter/material.dart';
import 'package:boilerplate/helpers/constants.dart';
import 'package:boilerplate/helpers/extensions.dart';

enum ImagePosition { top, left }

class FormBox extends StatelessWidget {
  final List<Widget> subtitles;
  final List<Widget> inputFields;
  final Function backRoute;
  final bool showLogo;
  final bool showBackButton;
  final double height;
  final ImagePosition imagePosition;
  final double width;
  final Widget submitButton;
  final Widget additional;
  final Key formKey;
  Color color;

  FormBox(
      {@required this.formKey,
        this.subtitles,
      this.inputFields,
      this.backRoute,
      this.submitButton,
      this.additional,
      this.color,
      this.showBackButton = false,
      this.showLogo = false,
      this.height = 5,
      this.imagePosition = ImagePosition.top,
      this.width = double.infinity});

  @override
  Widget build(BuildContext context) {
    double scalingWidth = this.width == double.infinity ? this.width : 1.scl * this.width;
    double scalingHeight = 1.scl * this.height;
    if(color == null) color = ColorHelper.accentColor;

    Widget _backButton() {
      return FlatButton.icon(
          padding: EdgeInsets.all(0),
          icon: Icons.arrow_back_ios.asIcon(false),
          label: Text(
            'Back',
            style: TypographyHelper.button().copyWith(color: Colors.black),
          ),
          onPressed: backRoute);
    }

    Widget _box() {
      return Column(
        children: <Widget>[
          ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              child: Container(
                width: (width != null) ? scalingWidth : scalingWidth,
                color: color,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      padding: const EdgeInsets.all(0),
                      margin: const EdgeInsets.all(16),
                      width: double.infinity,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
//                          width: 200,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Column(
                                  crossAxisAlignment:
                                  CrossAxisAlignment.start,
                                  children:
                                  subtitles == null ? [] : subtitles,
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          if(inputFields != null) Form(
                            key: formKey,
                            child: Column(
                              children: inputFields,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              )),
          const SizedBox(
            height: 15,
          ),
          if (additional != null) additional,
          if (additional != null && submitButton != null)
            const SizedBox(
              height: 15,
            ),
          if (submitButton != null) submitButton,
        ],
      );
    }

    Widget _imageTop() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          if(showLogo) logoImage2 == null ? Placeholder(color: ColorHelper.materialColor.shade500,).boxed(height: 10.scl, width: 10.scl,).uiPaddingOnly(t: 2) :  Image.asset(logoImage2, width: 9.scl, fit: BoxFit.fill,),
          if (showLogo)
            SizedBox(height: 0.1.scl,),
          SizedBox(
            height: 1.scl,
          ),
          if (showBackButton) _backButton(),
          _box()
        ],
      );
    }

    Widget _imageLeft() {
      return Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  if (showLogo)
                    Image.asset(logoImage2, width: 9.scl, fit: BoxFit.fill,),
                  if (showBackButton) _backButton(),
                ],
              )
            ],
          ),
          const SizedBox(
            width: 20,
          ),
          _box().expanded()
        ],
      );
    }

    return Container(
      width: width,
      child: imagePosition == ImagePosition.top ? _imageTop() : _imageLeft(),
    );
  }
}
