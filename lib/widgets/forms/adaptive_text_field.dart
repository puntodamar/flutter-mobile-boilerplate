import 'package:boilerplate/helpers/color_helper.dart';
import 'package:boilerplate/helpers/shape_helper.dart';
import 'package:boilerplate/helpers/validation_helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:boilerplate/helpers/extensions.dart';
import 'dart:io' show Platform;

class AdaptiveTextField extends StatelessWidget {
  final Widget prefix;
  final Widget suffix;
  final String hintText;
  final bool enabled;
  final FocusNode focusNode;
  final FocusNode nextField;
  final Function onSaved;
  final Function onTap;
  final Function onEditingComplete;
  final String dataKey;
  final bool autoFocus;
  final List<Validation> validators;
  final String initialValue;
  final int maxLength;
  final TextInputType keyboardType;
  final bool readOnly;
  final bool obscureText;
  final String obscureCharacter;
  final TextInputShape shape;
  final Color containerColor;

  TextEditingController controller;
  bool forceAndroid;
  bool forceIos;

  AdaptiveTextField(
      {@required this.dataKey,
      this.suffix,
      this.prefix,
      this.hintText,
      this.focusNode,
      this.nextField,
      this.onSaved,
      this.onEditingComplete,
      this.onTap,
      this.validators,
      this.initialValue,
      this.maxLength,
      this.controller,
      this.obscureText = false,
      this.obscureCharacter = '*',
      this.autoFocus = false,
      this.enabled = true,
      this.readOnly = false,
      this.containerColor,
      this.shape = TextInputShape.rectangle,
      this.keyboardType = TextInputType.text}) {
    this.forceAndroid = false;
    this.forceIos = false;
  }

  OverlayVisibilityMode overlayVisibilityMode = OverlayVisibilityMode.always;

  AdaptiveTextField.ios({
    @required this.dataKey,
    this.overlayVisibilityMode,
    this.suffix,
    this.prefix,
    this.hintText,
    this.focusNode,
    this.nextField,
    this.onSaved,
    this.onEditingComplete,
    this.onTap,
    this.validators,
    this.initialValue,
    this.maxLength,
    this.controller,
    this.obscureText = false,
    this.obscureCharacter = '*',
    this.autoFocus = false,
    this.enabled = true,
    this.readOnly = false,
    this.containerColor,
    this.keyboardType = TextInputType.text,
    this.shape = TextInputShape.rectangle,
  }) {
    this.forceIos = true;
    this.forceAndroid = false;
  }

  String labelText;
  String helperText;
  String errorText;
  String counterText;
  FloatingLabelBehavior floatingLabelBehavior;

  AdaptiveTextField.android({
    @required this.dataKey,
    this.prefix,
    this.suffix,
    this.hintText,
    this.focusNode,
    this.nextField,
    this.onSaved,
    this.onEditingComplete,
    this.onTap,
    this.validators,
    this.initialValue,
    this.maxLength,
    this.labelText,
    this.errorText,
    this.helperText,
    this.counterText,
    this.controller,
    this.containerColor,
    this.obscureText = false,
    this.obscureCharacter = '*',
    this.enabled = true,
    this.autoFocus = false,
    this.readOnly = false,
    this.floatingLabelBehavior = FloatingLabelBehavior.auto,
    this.keyboardType = TextInputType.text,
    this.shape = TextInputShape.rectangle,
  }) {
    this.forceIos = false;
    this.forceAndroid = true;
  }

  @override
  Widget build(BuildContext context) {
    if (controller == null) controller = TextEditingController();
    Color _cursorColor = ColorHelper.materialColor.shade900;
    Widget textField;
    var focusedBorder;
    var enabledBorder;
    var errorBorder;

    if (shape == TextInputShape.rectangle) {
      focusedBorder = ShapeHelper.textFieldRectangle;
      enabledBorder = ShapeHelper.textFieldRectangle;
      errorBorder = ShapeHelper.textFieldErrorRectangle;
    } else if (shape == TextInputShape.rounded) {
      focusedBorder = ShapeHelper.textFieldRounded;
      enabledBorder = ShapeHelper.textFieldRounded;
      errorBorder = ShapeHelper.textFieldErrorRounded;
    }

    if (Platform.isIOS && !forceAndroid || forceIos) {
      textField = CupertinoTextField(
        placeholderStyle: TextStyle(color: ColorHelper.materialColor.shade100),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            color: Colors.white,
            shape: BoxShape.rectangle),
        obscureText: obscureText,
        obscuringCharacter: obscureCharacter,
        prefixMode: overlayVisibilityMode,
        suffixMode: overlayVisibilityMode,
        readOnly: readOnly,
        cursorColor: _cursorColor,
        controller: controller,
        keyboardType: keyboardType,
        autofocus: autoFocus,
        focusNode: focusNode,
        enabled: enabled,
        placeholder: hintText,
        prefix: prefix,
        suffix: suffix,
        onSubmitted: (String value) {
          if (nextField != null)
            FocusScope.of(Get.context).requestFocus(nextField);
        },
      );
    } else {

      textField = Container(
        color: containerColor ?? ColorHelper.materialColor.shade50,
        child: TextFormField(
          onEditingComplete: onEditingComplete,
          onTap: onTap,
          obscuringCharacter: obscureCharacter,
          obscureText: obscureText,
          readOnly: readOnly,
          cursorColor: _cursorColor,
          controller: controller,
          keyboardType: keyboardType,
          initialValue: initialValue,
          focusNode: focusNode,
          decoration: InputDecoration(
              contentPadding: EdgeInsets.only(left: 20),
              fillColor: Colors.white,
              filled: true,
              focusedBorder: focusedBorder,
              enabledBorder: enabledBorder,
              errorBorder: errorBorder,
              focusedErrorBorder: errorBorder,
              floatingLabelBehavior: floatingLabelBehavior,
              helperText: helperText,
              counterText: (counterText != null)
                  ? "${controller.text.length} $counterText"
                  : "",
              errorText: errorText,
              labelText: labelText,
              labelStyle: TextStyle(
                  color: (focusNode != null && focusNode.hasFocus)
                      ? ColorHelper.accentColor
                      : ColorHelper.materialColor.shade900),
              enabled: enabled,
              hintText: hintText,
              prefixIcon: (prefix is Icon) ? prefix : null,
              prefix: (prefix is Icon) ? null : prefix,
              suffixIcon: (suffix is Icon) ? suffix : null,
              suffix: (suffix is Icon) ? null : suffix),
          validator: (value) {
            if (validators != null) {
              String errorMessage;
              if (validators.contains(Validation.empty)) {
                errorMessage = ValidationHelper.isEmpty(value, labelText);
              }
              if (validators.contains(Validation.email)) {
                errorMessage = ValidationHelper.validateEmail(value, labelText);
              }

              return errorMessage;
            }

            return null;
          },
          onFieldSubmitted: (String value) {
            if (nextField != null)
              FocusScope.of(Get.context).requestFocus(nextField);
          },
          onSaved: (String value) {
            onSaved(dataKey, value);
          },
        ),
      );
    }

    return forceAndroid ? textField.forceAsMaterialWidget() : textField;
  }
}
