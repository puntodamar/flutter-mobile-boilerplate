import 'package:boilerplate/helpers/color_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:boilerplate/helpers/extensions.dart';

// ignore: must_be_immutable
class AdaptiveSlider extends StatefulWidget {
  final Function onChanged;
  final Widget prefix;
  final Widget suffix;

  final double min;
  final double max;

  int divison;

  String labelText;
  bool useLabel;
  bool androidSlider;
  bool isRangeSlider;
  Color backgroundColor;

  double currentValue;
  RangeValues rangeValues;
  RangeLabels rangeLabels;

  AdaptiveSlider({
    this.onChanged,
    this.prefix,
    this.suffix,
    this.min = 0,
    this.max = 1,
  }) {
    this.androidSlider = false;
    this.isRangeSlider = false;
  }

  AdaptiveSlider.android({
    this.onChanged,
    this.prefix,
    this.suffix,
    this.divison,
    this.labelText,
    this.min = 0,
    this.max = 1,
    this.useLabel = false,
    this.backgroundColor = Colors.white,
  }) {
    this.androidSlider = true;
    this.isRangeSlider = false;
  }

  AdaptiveSlider.range({
    @required this.rangeValues,
    @required this.min,
    @required this.max,
    this.rangeLabels,
    this.onChanged,
    this.prefix,
    this.suffix,
    this.useLabel = false,
    this.backgroundColor = Colors.white,
  }) {
    this.isRangeSlider = true;
    this.androidSlider = true;
  }

  @override
  _AdaptiveSliderState createState() => _AdaptiveSliderState();
}

class _AdaptiveSliderState extends State<AdaptiveSlider> {
  double currentValue = 0;

  @override
  Widget build(BuildContext context) {
    if (widget.min != null && widget.max == null ||
        widget.max != null && widget.min == null)
      throw "min and max parameter should be both present";

    Widget _rangeSlider() {
      return RangeSlider(
        activeColor: ColorHelper.materialSliderData().activeColor,
        inactiveColor: ColorHelper.materialSliderData().inactiveColor,
        values: widget.rangeValues,
        min: widget.min,
        max: widget.max,
        divisions: 3,
        labels: RangeLabels("${widget.rangeValues.start.round()}",
            "${widget.rangeValues.end.round()}"),
        onChanged: (RangeValues value) {
          setState(() {
            widget.rangeValues = value;
            if(widget.onChanged == null)
              print("Slider current value: ${widget.rangeValues}");
          });
        },
      ).forceAsMaterialWidget(backgroundColor: widget.backgroundColor);
    }

    PlatformSlider _adaptiveSlider() {
      return PlatformSlider(
          min: widget.min,
          max: widget.max,
          divisions: widget.divison,
          onChanged: (double value) {
            setState(() {
              currentValue = value;
              widget.currentValue = currentValue;
              if (widget.onChanged == null) {
                print("Slider current value: $currentValue");
              } else
                widget.onChanged();
            });
          },
          value: currentValue,
          material: (_, __) => ColorHelper.materialSliderData(),
          cupertino: (_, __) => ColorHelper.cupertinoSliderData());
    }

    Widget _androidSlider() {
      String labelText = widget.labelText;
      return Slider(
          divisions: widget.divison,
          max: widget.max,
          min: widget.min,
          value: currentValue,
          onChanged: (double value) {
            setState(() {
              currentValue = value;
              widget.currentValue = currentValue;
              if (widget.onChanged == null) {
                print("Slider current value: $currentValue");
              } else
                widget.onChanged();
            });
          },
          label: widget.useLabel
              ? widget.labelText != null
              ? "$labelText: $currentValue"
              : "$currentValue"
              : null).forceAsMaterialWidget(backgroundColor: widget.backgroundColor);
    }

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        if (widget.prefix != null) widget.prefix,
        Expanded(child: widget.isRangeSlider
            ? _rangeSlider()
            : widget.androidSlider ? _androidSlider() : _adaptiveSlider(),),
        if (widget.suffix != null) widget.suffix
      ],
    );


    // return Row(
    //   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    //   children: <Widget>[
    //     if (widget.prefix != null) widget.prefix,
    //     Expanded(child: widget.isRangeSlider
    //         ? _rangeSlider()
    //         : widget.androidSlider ? _androidSlider() : _adaptiveSlider(),),
    //     if (widget.suffix != null) widget.suffix
    //   ],
    // );
  }
}
