import 'package:boilerplate/helpers/color_helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

enum PlatformCircularProgressIndicatorType {
  Android, iOS, Adaptive
}

// ignore: must_be_immutable
class AdaptiveCircularProgressIndicator extends StatelessWidget {

  PlatformCircularProgressIndicatorType platformCircularProgressIndicatorType;

  AdaptiveCircularProgressIndicator(){
    this.platformCircularProgressIndicatorType = PlatformCircularProgressIndicatorType.Adaptive;
  }

  AdaptiveCircularProgressIndicator.android(){
    this.platformCircularProgressIndicatorType = PlatformCircularProgressIndicatorType.Android;
  }

  AdaptiveCircularProgressIndicator.iOS(){
    this.platformCircularProgressIndicatorType = PlatformCircularProgressIndicatorType.iOS;
  }

  @override
  Widget build(BuildContext context) {

    if (platformCircularProgressIndicatorType == PlatformCircularProgressIndicatorType.Adaptive) {
      return GetPlatform.isIOS
          ? CupertinoActivityIndicator()
          : CircularProgressIndicator(
        valueColor: new AlwaysStoppedAnimation<Color>(
            ColorHelper.materialColor.shade500),
        backgroundColor: ColorHelper.materialColor.shade100,
      );
    }
    else if (platformCircularProgressIndicatorType == PlatformCircularProgressIndicatorType.Android) {
      return CircularProgressIndicator();
    }
    else if (platformCircularProgressIndicatorType == PlatformCircularProgressIndicatorType.iOS) {
      return CupertinoActivityIndicator();
    }

    return Container();


    // return SizedBox(
    //   height: 10,
    //   child: PlatformCircularProgressIndicator(
    //     material: (_, __)  => ColorHelper.materialProgressIndicatorData(),
    //     cupertino: (_, __) => ColorHelper.cupertinoProgressIndicatorData(),
    //   ),
    // );
  }
}
