import 'package:boilerplate/controllers/nav_drawer_controller.dart';
import 'package:boilerplate/helpers/color_helper.dart';
import 'package:boilerplate/helpers/navigation_helper.dart';
import 'package:flutter/material.dart';
import 'package:boilerplate/helpers/extensions.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';

import 'package:get/get.dart';

// ignore: must_be_immutable
class NavBarListItem extends StatelessWidget {
  final String title;
  final String routeDestination;
  final GlobalKey navigationKey;
  final int index;
  bool isExpanded;

  NavBarListItem(
      {@required this.title,
      @required this.routeDestination,
      @required this.navigationKey,
      @required this.index,
      this.isExpanded});

  @override
  Widget build(BuildContext context) {

    return GetBuilder<NavDrawerController>(
      builder: (c) => PlatformWidgetBuilder(
        cupertino: (_, child, __) {
          return Container(
              color: c.currentRouteName.value == routeDestination
                  ? ColorHelper.materialColor.shade500
                  : ColorHelper.materialColor.shade900,
              child: child
          );
        },
        material: (_, child, __) {
          return Material(
            color: c.currentRouteName.value == routeDestination
                ? ColorHelper.materialColor.shade500
                : ColorHelper.materialColor.shade900,
            child: child,
          );
        },
        child: ListTile(
          title: Text(
            title,
            style: TextStyle(
                color: Colors.white,
                fontSize: 1.7.fz,
                fontStyle: FontStyle.italic),
          ),
          onTap: () {
            if (c.currentRouteName.value != routeDestination) {
              c.updateNavigation(index, routeDestination);
            }

          },
        ),
      ),
    );
  }
}
