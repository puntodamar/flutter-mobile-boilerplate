import 'package:boilerplate/controllers/nav_drawer_controller.dart';
import 'package:boilerplate/helpers/color_helper.dart';
import 'package:boilerplate/helpers/device_info_helper.dart';
import 'package:boilerplate/helpers/navigation_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inner_drawer/inner_drawer.dart';
import 'package:boilerplate/helpers/extensions.dart';
import 'package:get/get.dart';

class AdaptiveDrawer extends StatefulWidget {

  final Widget scaffold;
  Widget navDrawer;
//  static GlobalKey<InnerDrawerState> innerDrawerKey = GlobalKey<InnerDrawerState>();
  AdaptiveDrawer({this.scaffold, this.navDrawer, Key key}) : super(key: key);

  @override
  AdaptiveDrawerState createState() => AdaptiveDrawerState();
}

class AdaptiveDrawerState extends State<AdaptiveDrawer> {

  GlobalKey<InnerDrawerState> innerDrawerKey = GlobalKey<InnerDrawerState>();
  void toggle() {
    innerDrawerKey.currentState.toggle(
      // direction is optional
      // if not set, the last direction will be used
      //InnerDrawerDirection.start OR InnerDrawerDirection.end
        direction: InnerDrawerDirection.start);
  }

  @override
  Widget build(BuildContext context) {
    // navDrawerController.setNavigationKey(innerDrawerKey);

    return InnerDrawer(
//        key: AdaptiveDrawer.innerDrawerKey,
        key: innerDrawerKey,
        onTapClose: true,
        // default false
        swipe: true,
        // default true
        colorTransitionChild: ColorHelper.accentColor,
        // default Color.black54
        colorTransitionScaffold: Colors.black54,
        // default Color.black54

        //When setting the vertical offset, be sure to use only top or bottom
        offset: IDOffset.horizontal(DeviceInfoHelper.isTablet || Get.context.isLandscape ? 0 : 0.5),
        scale: IDOffset.horizontal(0.8),
        // set the offset in both directions

        proportionalChildArea: true,
        // default true
        borderRadius: 50,
        // default 0
        leftAnimationType: InnerDrawerAnimation.quadratic,
        // default static
        rightAnimationType: InnerDrawerAnimation.static,
        backgroundDecoration: BoxDecoration(color: ColorHelper.materialColor.shade900),
        // default  Theme.of(context).backgroundColor

        //when a pointer that is in contact with the screen and moves to the right or left
        onDragUpdate: (double val, InnerDrawerDirection direction) {
          // return values between 1 and 0
//          print(val);
          // check if the swipe is to the right or to the left
//          print(direction == InnerDrawerDirection.start);
        },
        innerDrawerCallback: (opened) {
          // if (opened) NavigationHelper.jumpToIndex();
        },
        // return  true (open) or false (close)
        leftChild: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              widget.navDrawer.expanded()
            ]
        ).uiPaddingOnly(t: 50),
        // required if rightChild is not set
//        rightChild: Container(),
        // required if leftChild is not set

        //  A Scaffold is generally used but you are free to use other widgets
        // Note: use "automaticallyImplyLeading: false" if you do not personalize "leading" of Bar
        scaffold: widget.scaffold
    );


  }
}
