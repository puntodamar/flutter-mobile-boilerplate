import 'package:boilerplate/controllers/nav_drawer_controller.dart';
import 'package:boilerplate/helpers/navigation_helper.dart';
import 'package:boilerplate/helpers/typography_helper.dart';
import 'package:boilerplate/screens/expansion_tile_sample.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:boilerplate/helpers/color_helper.dart';
import 'package:boilerplate/helpers/extensions.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';

class NavDrawerItem extends StatelessWidget {
  final bool adaptive;

  NavDrawerItem(this.adaptive);

  @override
  Widget build(BuildContext context) {
    return adaptive
        ? Item(adaptive).forceAsMaterialWidget(
            backgroundColor: ColorHelper.materialColor.shade900)
        : Container(
            width: 0.7.sw,
            child: Drawer(
              child: Container(
                color: ColorHelper.materialColor.shade900,
                child: Item(adaptive),
//        child: _navDrawerList.paddingOnly(t: 3),
              ),
            ),
          );
  }
}

class Item extends StatelessWidget {
  final bool adaptive;

  Item(this.adaptive);

  @override
  Widget build(BuildContext context) {
    ItemScrollController itemScrollController = ItemScrollController();
    ItemPositionsListener _itemPositionsListener =
    ItemPositionsListener.create();

    List<dynamic> items;
    List<dynamic> drawerEntries = NavigationHelper.drawerEntries();

    if (Get.context.isPortrait) {
      Image image = Image.network(
        "https://picsum.photos/600/500",
        fit: BoxFit.fill,
      );

      Container header = Container(
        height: 0.3.sh,
        child: DrawerHeader(
          margin: EdgeInsets.zero,
          padding: EdgeInsets.zero,
          child: Stack(
            fit: StackFit.passthrough,
            children: [
              if (Get.context.isPortrait)
                adaptive
                    ? ClipRRect(
                    borderRadius:
                    BorderRadius.only(topRight: Radius.circular(40)),
                    child: image)
                    : image,
              Positioned.fill(
                  child: Text('Drawer Header',
                      style: TypographyHelper.headline2()
                          .copyWith(color: Colors.white))
                      .align(Alignment.bottomLeft)
                      .uiPaddingOnly(b: 10, l: 10))
            ],
          ),
        ),
      );
      items = [
        header,
        ..._toExpandableItem(
          drawerEntries,
        )
      ];
    } else {
      items = _toExpandableItem(drawerEntries);
    }

    drawerEntries.whereType<DrawerEntry>().forEach((element) {

    });

    itemScrollController = ItemScrollController();
    _itemPositionsListener = ItemPositionsListener.create();

    return ScrollablePositionedList.builder(
      itemCount: items.length,
      itemBuilder: (context, index) => items[index],
      itemScrollController: itemScrollController,
      itemPositionsListener: _itemPositionsListener,
    );
  }
}

List _toExpandableItem(List drawerEntries, [int nestCount = 1]) {
  List list = List<dynamic>();

  for (var entry in drawerEntries) {
    if (entry is DrawerEntry) {
      // list.add(Theme(
      //     data: ThemeData(
      //       unselectedWidgetColor: Colors.white,
      //       accentColor: Colors.white,
      //     ),
      //     child: GetBuilder<NavDrawerController>(
      //       builder: (c) {
      //         bool isExpanded =  c.selectedParentIndex.value == entry.index;
      //         return AppExpansionTile(
      //           key: PageStorageKey<DrawerEntry>(entry),
      //           initiallyExpanded: isExpanded,
      //           leading: entry.leading,
      //           title: Text(
      //             entry.title,
      //             style: TextStyle(
      //                 color: Colors.white,
      //                 fontWeight: FontWeight.bold,
      //                 fontSize: 2.fz),
      //           ),
      //           // backgroundColor: Colors.white,
      //           onExpansionChanged: (isExpanded) => {
      //             // if(isExpanded) setState(() {
      //             //   wawa.forEach((element) {
      //             //     if(element != parent1) element.currentState.collapse();
      //             //   });
      //             // })
      //           },
      //           children: List<Widget>.from(
      //               _toExpandableItem(entry.items, nestCount + 1)
      //                   .whereType<Widget>())
      //               .toList(),
      //         );
      //       },
      //     )
      //     ));

      list.add(GetBuilder<NavDrawerController>(
        builder: (c) {
          print("current route name: ${c.currentRouteName.value}");
          var routeSplit = c.currentRouteName.value.split("/");
          print(routeSplit);
          bool isExpanded = c.selectedParentIndex.value == entry.index || routeSplit[nestCount] == entry.title.toLowerCase().replaceAll(" ", "-");
          print("${entry.title.toLowerCase()} $isExpanded");
          GlobalKey<AppExpansionTileState> key = new GlobalKey();
          ExpansionTileParentData parentTile = ExpansionTileParentData(key: key, index: entry.index, parentIndex: entry.parentIndex);
          if (c.expansionTileParents.value.where((p) => p.index == parentTile.index).length == 0)
            c.addKey(parentTile);
          return AppExpansionTile(
            key: key,
            initiallyExpanded: isExpanded,
            leading: entry.leading,
            title: Text(
              entry.title,
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 2.fz),
            ),
            onExpansionChanged: (isExpanded) {

              if (isExpanded) {

                if(entry.isRoot) c.updateParentIndex(entry.index);

                c.expansionTileParents.value.forEach((p) {
                  if(p.index != entry.index && p.parentIndex == entry.parentIndex && p.key.currentState != null)
                    p.key.currentState.collapse();
                });
              }
            },
            children: List<Widget>.from(
                    _toExpandableItem(entry.items, nestCount + 1)
                        .whereType<Widget>())
                .toList(),
          );
        },
      ));
    } else
      list.add(entry);
  }

  return list.toList();
}
