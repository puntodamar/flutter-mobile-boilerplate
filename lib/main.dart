import 'package:boilerplate/controllers/auth_controller.dart';
import 'package:boilerplate/controllers/firebase_controller.dart';
import 'package:boilerplate/controllers/form_controller.dart';
import 'package:boilerplate/controllers/nav_drawer_controller.dart';
import 'package:boilerplate/controllers/notification_controller.dart';
import 'package:boilerplate/helpers/color_helper.dart';
import 'package:boilerplate/helpers/color_palette.dart';
import 'package:boilerplate/screens/example/firebase/audio_streaming_screen.dart';
import 'package:boilerplate/screens/example/firebase/firebase_auth_screen.dart';
import 'package:boilerplate/screens/example/firebase/firebase_database_screen.dart';
import 'package:boilerplate/screens/example/firebase/firebase_storage_screen.dart';
import 'package:boilerplate/screens/example/screens/adaptive_bottom_navbar_screen.dart';
import 'package:boilerplate/screens/example/screens/adaptive_single_screen.dart';
import 'package:boilerplate/screens/example/screens/android_top_navbar_screen.dart';
import 'package:boilerplate/screens/example/screens/auto_imply_leading_screen.dart';
import 'package:boilerplate/screens/example/screens/force_android_drawer_screen.dart';
import 'package:boilerplate/screens/example/screens/form_field_screen.dart';
import 'package:boilerplate/screens/example/screens/login_screen.dart';
import 'package:boilerplate/screens/example/screens/notification_screen.dart';
import 'package:boilerplate/screens/example/screens/radio_button_screen.dart';
import 'package:boilerplate/screens/example/screens/slider_screen.dart';
import 'package:boilerplate/screens/example/screens/splash_screen_example.dart';
import 'package:boilerplate/screens/example/screens/test.dart';
import 'package:boilerplate/screens/example/screens/without_boilerplate_screen.dart';
import 'package:boilerplate/screens/example/typography_screen.dart';
import 'package:boilerplate/screens/expansion_tile_sample.dart';
import 'package:boilerplate/screens/hero_screen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:get/get.dart';
import 'package:responsive_builder/responsive_builder.dart';

import 'controllers/image_picker_controller.dart';
import 'controllers/storage_controller.dart';
import 'screens/example/screens/splash_screen_example.dart';


Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await initServices();


  ResponsiveSizingConfig.instance.setCustomBreakpoints(
    ScreenBreakpoints(desktop: 950, tablet: 600, watch: 300),
  );
  MaterialColor materialColorSwatch = ColorPalette.generateMaterialColor();
  ColorHelper.materialColor = materialColorSwatch;

  runApp(GetMaterialApp(
    theme: ColorHelper.materialThemeData(),
    localizationsDelegates: [
      DefaultMaterialLocalizations.delegate,
      DefaultCupertinoLocalizations.delegate,
      DefaultWidgetsLocalizations.delegate,
    ],
    home: PlatformApp(
      localizationsDelegates: [
        DefaultMaterialLocalizations.delegate,
        DefaultCupertinoLocalizations.delegate,
        DefaultWidgetsLocalizations.delegate,
      ],
      onUnknownRoute: unKnownRoute,
      material: (_, __) =>
          MaterialAppData(theme: ColorHelper.materialThemeData()),
      cupertino: (_, __) =>
          CupertinoAppData(theme: ColorHelper.cupertinoThemeData()),
    ),
    // debugShowCheckedModeBanner: false,
    onUnknownRoute: unKnownRoute,
    initialRoute: SplashScreenExample.routeName,
    getPages: [
      GetPage(
          name: SplashScreenExample.routeName,
          page: () => SplashScreenExample()),
      GetPage(
          name: AdaptiveSingleScreen.routeName,
          page: () => AdaptiveSingleScreen()),
      GetPage(
          name: AdaptiveBottomNavbarScreen.routeName,
          page: () => AdaptiveBottomNavbarScreen()),
      GetPage(
          name: AndroidTopNavbarScreen.routeName,
          page: () => AndroidTopNavbarScreen()),
      GetPage(
          name: FormFieldScreen.routeName,
          page: () => FormFieldScreen()),
      GetPage(
          name: AutoImplyLeadingScreen.routeName,
          page: () => AutoImplyLeadingScreen()),
      GetPage(name: HeroScreen.routeName, page: () => HeroScreen()),
      GetPage(
          name: HeroDestinationScreen.routeName,
          page: () => HeroDestinationScreen()),
      GetPage(
          name: WithoutBoilerplateScreen.routeName,
          page: () => WithoutBoilerplateScreen()),
      GetPage(
          name: SliderScreen.routeName,
          page: () => SliderScreen()),
      GetPage(
          name: RadioButtonScreen.routeName,
          page: () => RadioButtonScreen()),
      GetPage(
          name: AutoImplyDestinationScreen.routeName,
          page: () => AutoImplyDestinationScreen()),
      GetPage(
          name: ForceAndroidDrawerScreen.routeName,
          page: () => ForceAndroidDrawerScreen()),
      GetPage(
        name: ExpansionTileSample.routeName,
        page: () => ExpansionTileSample(),
      ),
      GetPage(
        name: TypoGraphyScreen.routeName,
        page: () => TypoGraphyScreen(),
      ),
      GetPage(
        name: LoginScreen.routeName,
        page: () => LoginScreen(),
      ),
      GetPage(
        name: FirebaseProfileScreen.routeName,
        page: () => FirebaseProfileScreen(),
      ),
      GetPage(
        name: FirebaseDatabaseScreen.routeName,
        page: () => FirebaseDatabaseScreen(),
      ),
      GetPage(
        name: AudioStreamingScreen.routeName,
        page: () => AudioStreamingScreen(),
      ),
      GetPage(
        name: FirebaseStorageScreen.routeName,
        page: () => FirebaseStorageScreen(),
      ),
      GetPage(
        name: NotificationScreen.routeName,
        page: () => NotificationScreen(),
      ),
    ],
    // theme: GetPlatform.isIOS ? CupertinoAppData(theme: ColorHelper.cupertinoThemeData()),
  ));
}

Future<void> initServices() async {
  // await FlutterDownloader.initialize(
  //     debug: true // optional: set false to disable printing logs to console
  // );
  await Firebase.initializeApp();
  NotificationController notificationController = Get.put(NotificationController(), permanent: true);
  notificationController.initialize();
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return PlatformApp(
      home: SplashScreenExample(),
      material: (_, __) =>
          MaterialAppData(theme: ColorHelper.materialThemeData()),
      cupertino: (_, __) =>
          CupertinoAppData(theme: ColorHelper.cupertinoThemeData()),
    );
  }
}

Route unKnownRoute(RouteSettings settings){
  return new PageRouteBuilder(
      pageBuilder: (BuildContext context,Animation<double> animation,
          Animation<double> secondaryAnimation){
        return new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              const Text("First Page",textDirection: TextDirection.ltr,),
              const Padding(padding: const EdgeInsets.all(10.0)),
              new GestureDetector(
                onTap: () => Navigator.of(context).pop(),
                child: new Container(
                  padding: const EdgeInsets.all(10.0),
                  color:Colors.blue,
                  child: const Text("Back"),
                ),
              )
            ]
        );
      }
  );
}
